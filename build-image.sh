#docker stop clinica-backend-container
#docker rmi wilvermartinezg/clinica-backend:dev
#cd projects/umg-clinica-ws
#git pull
#./mvnw clean install
#docker build -t wilvermartinezg/clinica-backend:dev .
#docker run \
#    --name clinica-backend-container \
#    --rm \
#    -d \
#    -p 8080:8080 \
#    -e "TZ=America/Guatemala" \
#    wilvermartinezg/clinica-backend:dev

#function dev() {
##  docker build --platform linux/amd64,linux/arm64 -t wilvermartinezg/clinica-backend:dev .
#  docker build --platform linux/amd64 -t wilvermartinezg/clinica-backend:dev .
#}

function dev() {
  ./mvnw clean install \
  && docker build --platform linux/amd64 -t wilvermartinezg/clinica-backend:dev .
}

"$@"