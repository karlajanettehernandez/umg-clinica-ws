create database clinica_medica;

create schema if not exists usr;

create table if not exists usr.user
(
    id          bigserial,
    active      bool,
    created_at  timestamp,
    created_by  varchar(50),
    modified_at timestamp,
    modified_by varchar(50),
    name        varchar(50),
    email       varchar(50),
    password    varchar(100),
    constraint user_pkey primary key (id)
);

create table if not exists usr.role
(
    id     bigserial,
    code   varchar(50),
    name   varchar(50),
    active bool,
    constraint role_pkey primary key (id)
);

create table if not exists usr.role_user
(
    id          bigserial,
    active      bool,
    created_at  timestamp,
    created_by  varchar(50),
    modified_at timestamp,
    modified_by varchar(50),
    user_id     bigint,
    role_id     bigint,
    constraint role_user_pkey primary key (id),
    constraint role_user_to_user foreign key (user_id) references usr.user (id),
    constraint role_user_to_role foreign key (role_id) references usr.role (id)
);

create schema if not exists person;

create table if not exists person.gender
(
    id     serial,
    name   varchar(50),
    active bool,
    constraint person_gender_pkey primary key (id)
);

create table if not exists person.document_type
(
    id     serial,
    code   varchar(10),
    name   varchar(50),
    active bool,
    constraint person_document_type_pkey primary key (id)
);

create schema if not exists clinic;

create table if not exists clinic.doctor_specialty
(
    id     bigserial,
    name   varchar(50),
    active bool,
    constraint doctor_specialty_pkey primary key (id)
);

create table if not exists clinic.specialty
(
    id     bigserial,
    name   varchar(50),
    active bool,
    constraint specialty_pkey primary key (id)
);

create table if not exists clinic.service_type
(
    id     bigserial,
    name   varchar(50),
    active bool,
    constraint service_type_pkey primary key (id)
);

create table if not exists clinic.doctor
(
    id                              bigserial,
    active                          bool,
    created_at                      timestamp,
    created_by                      varchar(50),
    modified_at                     timestamp,
    modified_by                     varchar(50),
    name                            varchar(50),
    last_name                       varchar(50),
    email                           varchar(50),
    birth_date                      date,
    gender_id                       integer,
    specialty_id                    integer,
    phone_number                    varchar(50),
    address                         varchar(100),
    active_collegiate               varchar(15),
    identification_document_number  varchar(30),
    identification_document_type_id bigint,
    constraint doctor_pkey primary key (id),
    constraint doctor_to_person_gender_fkey foreign key (gender_id) references person.gender (id),
    constraint doctor_to_specialty_fkey foreign key (specialty_id) references clinic.specialty (id),
    constraint doctor_to_person_document_type_fkey foreign key (identification_document_type_id) references person.document_type (id)
);

create table if not exists clinic.patient
(
    id                              bigserial,
    active                          bool,
    created_at                      timestamp,
    created_by                      varchar(50),
    modified_at                     timestamp,
    modified_by                     varchar(50),
    name                            varchar(50),
    last_name                       varchar(50),
    email                           varchar(50),
    birth_date                      date,
    gender_id                       integer,
    phone_number                    varchar(50),
    address                         varchar(100),
    identification_document_number  varchar(30),
    identification_document_type_id bigint,
    constraint patient_pkey primary key (id),
    constraint patient_to_person_gender foreign key (gender_id) references person.gender (id),
    constraint patient_to_person_document_type foreign key (identification_document_type_id) references person.document_type (id)
);

create table if not exists clinic.medical_schedule_status
(
    id     bigserial,
    code   varchar(50),
    name   varchar(50),
    active bool,
    constraint medical_schedule_status_pkey primary key (id)
);

create table if not exists clinic.medical_schedule
(
    id                         bigserial,
    active                     bool,
    created_at                 timestamp,
    created_by                 varchar(50),
    modified_at                timestamp,
    modified_by                varchar(50),
    note                       varchar(500),
    start_at                   timestamp,
    end_at                     timestamp,
    medical_schedule_status_id bigint,
    doctor_id                  bigint,
    patient_id                 bigint,
    constraint medical_schedule_pkey primary key (id),
    constraint medical_schedule_to_medical_schedule_status foreign key (medical_schedule_status_id) references clinic.medical_schedule_status (id),
    constraint medical_schedule_to_doctor foreign key (doctor_id) references clinic.doctor (id),
    constraint medical_schedule_to_patient foreign key (patient_id) references clinic.patient (id)
);

create table if not exists clinic.medical_history
(
    id              bigserial,
    active          bool,
    created_at      timestamp,
    created_by      varchar(50),
    modified_at     timestamp,
    modified_by     varchar(50),
    doctor_id       bigint,
    patient_id      bigint,
    specialty_id    bigint,
    service_type_id bigint,
    description     text,
    constraint medical_history_pkey primary key (id),
    constraint medical_history_to_doctor foreign key (doctor_id) references clinic.doctor (id),
    constraint medical_history_to_patient foreign key (patient_id) references clinic.patient (id)
);

create table if not exists clinic.lab_test
(
    id          bigserial,
    active      bool,
    created_at  timestamp,
    created_by  varchar(50),
    modified_at timestamp,
    modified_by varchar(50),
    name        varchar(100),
    constraint lab_test_pkey primary key (id)
);

create table if not exists clinic.medical_history_lab_test
(
    id                 bigserial,
    active             bool,
    created_at         timestamp,
    created_by         varchar(50),
    modified_at        timestamp,
    modified_by        varchar(50),
    lab_test_id        bigint,
    medical_history_id bigint,
    constraint medical_history_lab_test_pkey primary key (id),
    constraint medical_history_lab_test_to_lab_test foreign key (lab_test_id) references clinic.lab_test (id),
    constraint medical_history_lab_test_to_medical_history foreign key (medical_history_id) references clinic.medical_history (id)
);

create table if not exists clinic.medicine
(
    id          bigserial,
    active      bool,
    created_at  timestamp,
    created_by  varchar(50),
    modified_at timestamp,
    modified_by varchar(50),
    name        varchar(100),
    constraint medicine_pkey primary key (id)
);

create table if not exists clinic.medical_history_medication
(
    id                 bigserial,
    active             bool,
    created_at         timestamp,
    created_by         varchar(50),
    modified_at        timestamp,
    modified_by        varchar(50),
    description        varchar(100),
    indication         varchar(200),
    medicine_id        bigint,
    medical_history_id bigint,
    constraint medical_history_medication_pkey primary key (id),
    constraint medical_history_medication_to_medicine foreign key (medicine_id) references clinic.medicine (id),
    constraint medical_history_medication_to_medical_history foreign key (medical_history_id) references clinic.medical_history (id)
);
