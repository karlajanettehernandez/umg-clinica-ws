package gt.umg.clinica.api.controller.patient.find

import gt.umg.clinica.api.controller.patient.PatientJsonDto
import gt.umg.clinica.domain.repository.PatientRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

@RestController
class FindPatientByIdController {

    @Autowired
    private lateinit var patientRepository: PatientRepository

    @GetMapping(value = ["/api/patient/{patientId}"])
    fun index(@PathVariable(value = "patientId") patientId: Long): ResponseEntity<PatientJsonDto> {
        val result = patientRepository.findById(patientId)

        return if (result.isPresent) {
            ResponseEntity.ok(PatientJsonDto.fromHibernateEntity(result.get()))
        } else {
            ResponseEntity.ok(PatientJsonDto())
        }
    }

}
