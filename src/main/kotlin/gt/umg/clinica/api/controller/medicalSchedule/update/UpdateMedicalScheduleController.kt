package gt.umg.clinica.api.controller.medicalSchedule.update

import gt.umg.clinica.api.controller.medicalSchedule.MedicalScheduleJsonDto
import gt.umg.clinica.application.medicalSchedule.MedicalScheduleRequest
import gt.umg.clinica.application.medicalSchedule.update.MedicalScheduleUpdater
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.lang.RuntimeException
import java.time.LocalDateTime

@RestController
@RequestMapping(value = ["/api/medical-schedule"])
class UpdateMedicalScheduleController {

    @Autowired
    private lateinit var medicalScheduleUpdater: MedicalScheduleUpdater

    @PutMapping
    @Transactional(rollbackFor = [RuntimeException::class])
    fun index(
        @RequestBody data: MedicalScheduleJsonDto
    ): ResponseEntity<Unit> {
        val request = MedicalScheduleRequest.build {
            this.id = data.id ?: 0L
            this.note = data.note ?: ""
            this.startAt = data.startAt ?: LocalDateTime.now()
            this.endAt = data.endAt ?: LocalDateTime.now()
            this.doctorId = data.doctorId ?: 0L
            this.patientId = data.patientId ?: 0L
            this.medicalScheduleStatusId = data.medicalScheduleStatusId ?: 0L
        }

        medicalScheduleUpdater.update(request)

        return ResponseEntity.ok(Unit)
    }

}
