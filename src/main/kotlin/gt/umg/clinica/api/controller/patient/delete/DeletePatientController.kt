package gt.umg.clinica.api.controller.patient.delete

import gt.umg.clinica.application.patient.delete.PatientDeleter
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.lang.RuntimeException

@RestController
@RequestMapping(value = ["/api/patient"])
class DeletePatientController {

    @Autowired
    private lateinit var patientDeleter: PatientDeleter

    @DeleteMapping(value = ["/{id}"])
    @Transactional(rollbackFor = [RuntimeException::class])
    fun index(@PathVariable(value = "id") id: Long): ResponseEntity<Unit> {
        patientDeleter.delete(id)
        return ResponseEntity.ok(Unit)
    }

}
