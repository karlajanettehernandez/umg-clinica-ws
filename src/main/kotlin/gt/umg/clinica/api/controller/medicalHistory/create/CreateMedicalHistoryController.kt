package gt.umg.clinica.api.controller.medicalHistory.create

import gt.umg.clinica.api.controller.medicalHistory.MedicalHistoryJsonDto
import gt.umg.clinica.application.medicalHistory.create.MedicalHistoryCreator
import gt.umg.clinica.application.medicalHistory.MedicalHistoryRequest
import gt.umg.clinica.application.medicalHistory.create.MedicalHistoryInventoryReferenceCreator
import gt.umg.clinica.application.medicalHistory.create.MedicalHistoryLabTestCreator
import gt.umg.clinica.application.medicalHistory.create.MedicalHistoryPrescriptionCreator
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class CreateMedicalHistoryController {

    @Autowired
    private lateinit var medicalHistoryCreator: MedicalHistoryCreator

    @Autowired
    private lateinit var medicalHistoryLabTestCreator: MedicalHistoryLabTestCreator

    @Autowired
    private lateinit var medicalHistoryPrescriptionCreator: MedicalHistoryPrescriptionCreator

    @Autowired
    private lateinit var medicalHistoryInventoryReferenceCreator: MedicalHistoryInventoryReferenceCreator

    @PostMapping(value = ["/api/medical-history"])
    fun index(@RequestBody data: MedicalHistoryJsonDto): ResponseEntity<Long> {
        val medicalHistoryRequest = MedicalHistoryRequest.build {
            this.doctorId = data.doctorId
            this.patientId = data.patientId
            this.description = data.description
            this.diagnostic = data.diagnostic
        }

        val id = medicalHistoryCreator.create(medicalHistoryRequest)

        medicalHistoryLabTestCreator.create(id, data.labTestList ?: listOf())
        medicalHistoryPrescriptionCreator.create(id, data.prescriptionList ?: listOf())
        medicalHistoryInventoryReferenceCreator.create(id, data.inventoryReferenceList ?: listOf())

        return ResponseEntity.status(HttpStatus.CREATED).body(id)
    }

}
