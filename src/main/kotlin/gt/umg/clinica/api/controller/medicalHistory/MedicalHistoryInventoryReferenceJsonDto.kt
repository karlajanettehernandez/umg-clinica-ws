package gt.umg.clinica.api.controller.medicalHistory

class MedicalHistoryInventoryReferenceJsonDto {
    var id: Long? = null
    var medicineInventoryId: Long? = null
    var medicineName: String? = null
    var quantity: Int? = null

    companion object {
        fun build(init: MedicalHistoryInventoryReferenceJsonDto.() -> Unit) =
            MedicalHistoryInventoryReferenceJsonDto().apply(init)
    }
}
