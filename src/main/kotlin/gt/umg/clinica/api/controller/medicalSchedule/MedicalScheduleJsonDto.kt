package gt.umg.clinica.api.controller.medicalSchedule

import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import gt.umg.clinica.api.converters.CustomLocalDateTimeDeserializer
import gt.umg.clinica.api.converters.CustomLocalDateTimeSerializer
import java.time.LocalDateTime

class MedicalScheduleJsonDto {
    var id: Long? = 0L
    var note: String? = ""

    @JsonSerialize(using = CustomLocalDateTimeSerializer::class)
    @JsonDeserialize(using = CustomLocalDateTimeDeserializer::class)
    var startAt: LocalDateTime? = LocalDateTime.now()

    @JsonSerialize(using = CustomLocalDateTimeSerializer::class)
    @JsonDeserialize(using = CustomLocalDateTimeDeserializer::class)
    var endAt: LocalDateTime? = LocalDateTime.now()

    var medicalScheduleStatusId: Long? = 0L
    var medicalScheduleStatusName: String? = ""
    var doctorId: Long? = 0L
    var doctorName: String? = ""
    var patientId: Long? = 0L
    var patientName: String? = ""

    var createdBy: String? = ""

    companion object {
        fun build(init: MedicalScheduleJsonDto.() -> Unit) = MedicalScheduleJsonDto().apply(init)
    }
}
