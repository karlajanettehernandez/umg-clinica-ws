package gt.umg.clinica.api.controller.medicalSchedule

import gt.umg.clinica.application.medicalSchedule.delete.MedicalScheduleDeleter
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

@RestController
class MedicalScheduleController {

    @Autowired
    private lateinit var medicalScheduleDeleter: MedicalScheduleDeleter

    @DeleteMapping(value = ["/api/medical-schedule/{id}"])
    fun delete(@PathVariable(value = "id") id: Long): ResponseEntity<Unit> {
        medicalScheduleDeleter.delete(id)
        return ResponseEntity.ok(Unit)
    }

}
