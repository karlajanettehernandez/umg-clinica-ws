package gt.umg.clinica.api.controller.gender

class GenderJsonDto {
    var id: Long? = 0L
    var name: String? = ""

    companion object {
        fun build(init: GenderJsonDto.() -> Unit) = GenderJsonDto().apply(init)
    }
}
