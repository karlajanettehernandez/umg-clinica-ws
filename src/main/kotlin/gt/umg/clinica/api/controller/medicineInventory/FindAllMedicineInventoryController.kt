package gt.umg.clinica.api.controller.medicineInventory

import gt.umg.clinica.domain.repository.MedicineInventoryRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class FindAllMedicineInventoryController {

    @Autowired
    private lateinit var medicineInventoryRepository: MedicineInventoryRepository

    @GetMapping(value = ["/api/medicine-inventory/all"])
    fun findAll(): ResponseEntity<List<MedicineInventoryJsonDto>> {
        val result = medicineInventoryRepository
            .findallOrderByCreatedAtDesc()
            .map {
                MedicineInventoryJsonDto.build {
                    this.id = it.id
                    this.purchasePrice = it.purchasePrice
                    this.salePrice = it.salePrice
                    this.quantity = it.quantity
                    this.batchNumber = it.batchNumber
                    this.expirationDate = it.expirationDate
                    this.medicineId = it.medicine?.id
                    this.medicineName = it.medicine?.name
                    this.quantitySale = it.quantitySale
                    this.stock = it.stock
                    this.active = it.active
                    this.comments = it.comments
                }
            }

        return ResponseEntity.ok(result)
    }

    @GetMapping(value = ["/api/medicine-inventory/in-stock"])
    fun findInStock(): ResponseEntity<List<MedicineInventoryJsonDto>> {
        val result = medicineInventoryRepository
            .findAllActiveTrueAndStockGreaterThanZero()
            .map {
                MedicineInventoryJsonDto.build {
                    this.id = it.id
                    this.purchasePrice = it.purchasePrice
                    this.salePrice = it.salePrice
                    this.quantity = it.quantity
                    this.batchNumber = it.batchNumber
                    this.expirationDate = it.expirationDate
                    this.medicineId = it.medicine?.id
                    this.medicineName = it.medicine?.name
                    this.quantitySale = it.quantitySale
                    this.stock = it.stock
                    this.active = it.active
                }
            }

        return ResponseEntity.ok(result)
    }

}
