package gt.umg.clinica.api.controller.patient.create

import gt.umg.clinica.api.controller.patient.PatientJsonDto
import gt.umg.clinica.application.patient.PatientRequest
import gt.umg.clinica.application.patient.create.PatientCreator
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.lang.RuntimeException
import java.time.LocalDate

@RestController
@RequestMapping(value = ["/api/patient"])
class CreatePatientController {

    @Autowired
    private lateinit var patientCreator: PatientCreator

    @PostMapping
    @Transactional(rollbackFor = [RuntimeException::class])
    fun index(@RequestBody data: PatientJsonDto): ResponseEntity<Long> {
        val request = PatientRequest.build {
            this.name = data.name ?: ""
            this.lastName = data.lastName ?: ""
            this.email = data.email ?: ""
            this.birthDate = data.birthDate ?: LocalDate.now()
            this.genderId = data.genderId ?: 0L
            this.phoneNumber = data.phoneNumber ?: ""
            this.address = data.address ?: ""
            this.identificationDocumentNumber = data.identificationDocumentNumber ?: ""
            this.identificationDocumentTypeId = data.identificationDocumentTypeId ?: 0L
            this.active = data.active ?: false
            this.schedule = data.schedule ?: 0L
        }

        val result = patientCreator.create(request)

        return ResponseEntity.status(HttpStatus.CREATED).body(result)
    }

}
