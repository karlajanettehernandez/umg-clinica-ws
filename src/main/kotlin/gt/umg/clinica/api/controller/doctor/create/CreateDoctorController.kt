package gt.umg.clinica.api.controller.doctor.create

import gt.umg.clinica.api.controller.doctor.DoctorJsonDto
import gt.umg.clinica.application.doctor.DoctorRequest
import gt.umg.clinica.application.doctor.create.DoctorCreator
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.lang.RuntimeException
import java.time.LocalDate

@RestController
@RequestMapping(value = ["/api/doctor"])
class CreateDoctorController {

    @Autowired
    private lateinit var doctorCreator: DoctorCreator

    @PostMapping
    @Transactional(rollbackFor = [RuntimeException::class])
    fun index(@RequestBody data: DoctorJsonDto): ResponseEntity<Long> {
        val request = DoctorRequest.build {
            this.name = data.name ?: ""
            this.lastName = data.lastName ?: ""
            this.email = data.email ?: ""
            this.birthDate = data.birthDate ?: LocalDate.now()
            this.genderId = data.genderId ?: 0L
            this.phoneNumber = data.phoneNumber ?: ""
            this.address = data.address ?: ""
            this.activeCollegiate = data.activeCollegiate ?: ""
            this.identificationDocumentNumber = data.identificationDocumentNumber ?: ""
            this.identificationDocumentTypeId = data.identificationDocumentTypeId ?: 0L
            this.specialtyId = data.specialtyId ?: 0L
            this.active = data.active ?: false
        }

        val result = doctorCreator.create(request)

        return ResponseEntity.status(HttpStatus.CREATED).body(result)
    }

}
