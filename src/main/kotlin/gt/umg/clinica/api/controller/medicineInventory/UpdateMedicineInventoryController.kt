package gt.umg.clinica.api.controller.medicineInventory

import gt.umg.clinica.application.medicineInventory.MedicineInventoryRequest
import gt.umg.clinica.application.medicineInventory.MedicineInventoryUpdater
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class UpdateMedicineInventoryController {

    @Autowired
    private lateinit var medicineInventoryUpdater: MedicineInventoryUpdater

    @PutMapping(value = ["/api/medicine-inventory"])
    fun index(@RequestBody data: MedicineInventoryJsonDto): ResponseEntity<Unit> {

        val request = MedicineInventoryRequest.build {
            this.id = data.id
            this.purchasePrice = data.purchasePrice
            this.salePrice = data.salePrice
            this.quantity = data.quantity
            this.batchNumber = data.batchNumber
            this.expirationDate = data.expirationDate
            this.medicineId = data.medicineId
            this.active = data.active
        }

        medicineInventoryUpdater.update(request)

        return ResponseEntity.ok(Unit)
    }

}
