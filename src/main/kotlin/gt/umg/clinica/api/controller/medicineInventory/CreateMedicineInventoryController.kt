package gt.umg.clinica.api.controller.medicineInventory

import gt.umg.clinica.application.medicineInventory.MedicineInventoryCreator
import gt.umg.clinica.application.medicineInventory.MedicineInventoryRequest
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class CreateMedicineInventoryController {

    @Autowired
    private lateinit var medicineInventoryCreator: MedicineInventoryCreator

    @PostMapping(value = ["/api/medicine-inventory"])
    fun index(@RequestBody data: MedicineInventoryJsonDto): ResponseEntity<Long> {

        val request = MedicineInventoryRequest.build {
            this.purchasePrice = data.purchasePrice
            this.salePrice = data.salePrice
            this.quantity = data.quantity
            this.batchNumber = data.batchNumber
            this.expirationDate = data.expirationDate
            this.medicineId = data.medicineId
            this.medicineName = data.medicineName
        }

        val result = medicineInventoryCreator.create(request)

        return ResponseEntity.ok(result)
    }

}
