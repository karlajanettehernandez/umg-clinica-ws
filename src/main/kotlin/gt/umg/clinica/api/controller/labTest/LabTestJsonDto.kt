package gt.umg.clinica.api.controller.labTest

class LabTestJsonDto {
    var id: Long? = null
    var name: String? = null

    companion object {
        fun build(init: LabTestJsonDto.() -> Unit) = LabTestJsonDto().apply(init)
    }
}
