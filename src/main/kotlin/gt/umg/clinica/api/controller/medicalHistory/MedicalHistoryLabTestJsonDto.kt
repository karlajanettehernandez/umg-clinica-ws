package gt.umg.clinica.api.controller.medicalHistory

import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(value = JsonInclude.Include.NON_NULL)
class MedicalHistoryLabTestJsonDto {
    var id: Long? = null
    var labTestId: Long? = null
    var labTestName: String? = null
    var description: String? = null

    companion object {
        fun build(init: MedicalHistoryLabTestJsonDto.() -> Unit) = MedicalHistoryLabTestJsonDto().apply(init)
    }
}
