package gt.umg.clinica.api.controller.medicineInventory

import gt.umg.clinica.domain.exceptions.ValidationException
import gt.umg.clinica.domain.repository.MedicineInventoryRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class DeactivateMedicineInventoryController {

    @Autowired
    private lateinit var medicineInventoryRepository: MedicineInventoryRepository

    @PutMapping(value = ["/api/medicine-inventory/deactivate"])
    fun index(@RequestBody data: MedicineInventoryJsonDto): ResponseEntity<Unit> {
        val entity = medicineInventoryRepository
            .findById(data.id ?: 0L)
            .orElseThrow { ValidationException("Inventario no existe") }

        entity.active = false
        entity.comments = data.comments

        medicineInventoryRepository.save(entity)

        return ResponseEntity.ok(Unit)
    }

}
