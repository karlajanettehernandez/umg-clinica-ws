package gt.umg.clinica.api.controller.medicalHistory

import java.time.LocalDateTime

class MedicalHistoryJsonDto {
    var id: Long? = null
    var doctorId: Long? = null
    var doctorName: String? = null
    var patientId: Long? = null
    var patientName: String? = null
    var description: String? = null
    var diagnostic: String? = null
    var active: Boolean? = null
    var createdAt: LocalDateTime? = null
    var createdBy: String? = null
    var modifiedAt: LocalDateTime? = null
    var modifiedBy: String? = null
    var labTestList: List<MedicalHistoryLabTestJsonDto>? = null
    var prescriptionList: List<MedicalHistoryPrescriptionJsonDto>? = null
    var inventoryReferenceList: List<MedicalHistoryInventoryReferenceJsonDto>? = null

    companion object {
        fun build(init: MedicalHistoryJsonDto.() -> Unit) = MedicalHistoryJsonDto().apply(init)
    }
}
