package gt.umg.clinica.api.controller.doctor

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import gt.umg.clinica.api.converters.CustomLocalDateDeserializer
import gt.umg.clinica.api.converters.CustomLocalDateSerializer
import gt.umg.clinica.api.converters.CustomLocalDateTimeDeserializer
import gt.umg.clinica.api.converters.CustomLocalDateTimeSerializer
import java.time.LocalDate
import java.time.LocalDateTime

@JsonInclude(value = JsonInclude.Include.NON_NULL)
class DoctorJsonDto {
    var id: Long? = null
    var name: String? = null
    var lastName: String? = null
    var email: String? = null

    @JsonSerialize(using = CustomLocalDateSerializer::class)
    @JsonDeserialize(using = CustomLocalDateDeserializer::class)
    var birthDate: LocalDate? = null

    var genderId: Long? = null
    var genderName: String? = null
    var phoneNumber: String? = null
    var address: String? = null
    var activeCollegiate: String? = null
    var identificationDocumentNumber: String? = null
    var identificationDocumentTypeId: Long? = null
    var identificationDocumentTypeName: String? = null
    var specialtyId: Long? = null
    var specialtyName: String? = null
    var active: Boolean? = null

    @JsonSerialize(using = CustomLocalDateTimeSerializer::class)
    @JsonDeserialize(using = CustomLocalDateTimeDeserializer::class)
    var createdAt: LocalDateTime? = null
    var createdBy: String? = null

    @JsonSerialize(using = CustomLocalDateTimeSerializer::class)
    @JsonDeserialize(using = CustomLocalDateTimeDeserializer::class)
    var modifiedAt: LocalDateTime? = null
    var modifiedBy: String? = null

    companion object {
        fun build(init: DoctorJsonDto.() -> Unit) = DoctorJsonDto().apply(init)
    }
}
