package gt.umg.clinica.api.controller.role

class RoleMenuJsonDto {
    var menuId: Long? = null
    var permissions: List<String>? = null
}
