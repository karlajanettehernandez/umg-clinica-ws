package gt.umg.clinica.api.controller.medicalSchedule.find

import gt.umg.clinica.api.controller.medicalSchedule.MedicalScheduleJsonDto
import gt.umg.clinica.domain.repository.MedicalScheduleRepository
import gt.umg.clinica.domain.shared.MapperUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class FindAllMedicalScheduleController {

    @Autowired
    private lateinit var medicalScheduleRepository: MedicalScheduleRepository

    @GetMapping(value = ["/api/medical-schedule"])
    @Transactional(readOnly = true)
    fun index(): ResponseEntity<List<MedicalScheduleJsonDto>> {
        val result = medicalScheduleRepository
            .findAllActiveTrue()
            .map {
                MedicalScheduleJsonDto.build {
                    this.id = it.id
                    this.note = it.note
                    this.startAt = it.startAt
                    this.endAt = it.endAt
                    this.medicalScheduleStatusId = it.medicalScheduleStatus?.id
                    this.medicalScheduleStatusName = it.medicalScheduleStatus?.name
                    this.doctorId = it.doctor?.id
                    this.doctorName = MapperUtils.getDoctorName(it.doctor)
                    this.patientId = it.patient?.id
                    this.patientName = it.patientName
                    this.createdBy = it.createdBy
                }
            }

        return ResponseEntity.ok(result)
    }

}
