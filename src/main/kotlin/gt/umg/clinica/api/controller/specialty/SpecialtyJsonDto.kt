package gt.umg.clinica.api.controller.specialty

class SpecialtyJsonDto {
    var id: Long? = 0L
    var name: String? = ""

    companion object {
        fun build(init: SpecialtyJsonDto.() -> Unit) = SpecialtyJsonDto().apply(init)
    }
}
