package gt.umg.clinica.api.controller.labTest

import gt.umg.clinica.domain.repository.LabTestRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class GetLabTestListController {

    @Autowired
    private lateinit var labTestRepository: LabTestRepository

    @GetMapping(value = ["/api/lab-test/list"])
    fun index(): ResponseEntity<List<LabTestJsonDto>> {
        val result = labTestRepository
            .findAllActiveTrue()
            .map {
                LabTestJsonDto.build {
                    this.id = it.id
                    this.name = it.name
                }
            }

        return ResponseEntity.ok(result)
    }

}
