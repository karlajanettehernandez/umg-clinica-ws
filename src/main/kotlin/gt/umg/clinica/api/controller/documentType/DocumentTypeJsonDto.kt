package gt.umg.clinica.api.controller.documentType

class DocumentTypeJsonDto {
    var id: Long? = 0L
    var name: String? = ""

    companion object {
        fun build(init: DocumentTypeJsonDto.() -> Unit) = DocumentTypeJsonDto().apply(init)
    }
}
