package gt.umg.clinica.api.controller.medicalSchedule.create

import gt.umg.clinica.api.controller.medicalSchedule.MedicalScheduleJsonDto
import gt.umg.clinica.application.medicalSchedule.MedicalScheduleRequest
import gt.umg.clinica.application.medicalSchedule.create.MedicalScheduleCreator
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.lang.RuntimeException
import java.time.LocalDateTime

@RestController
@RequestMapping(value = ["/api/medical-schedule"])
class CreateMedicalScheduleController {

    @Autowired
    private lateinit var medicalScheduleCreator: MedicalScheduleCreator

    @PostMapping
    @Transactional(rollbackFor = [RuntimeException::class])
    fun index(
        @RequestBody data: MedicalScheduleJsonDto
    ): ResponseEntity<Long> {
        val request = MedicalScheduleRequest.build {
            this.note = data.note ?: ""
            this.startAt = data.startAt ?: LocalDateTime.now()
            this.endAt = data.endAt ?: LocalDateTime.now()
            this.doctorId = data.doctorId ?: 0L
            this.patientId = data.patientId ?: 0L
            this.patientName = data.patientName ?: ""
        }

        val result = medicalScheduleCreator.create(request)

        return ResponseEntity.status(HttpStatus.CREATED).body(result)
    }

}
