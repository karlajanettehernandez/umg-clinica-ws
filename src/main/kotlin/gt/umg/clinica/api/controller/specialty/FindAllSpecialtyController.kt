package gt.umg.clinica.api.controller.specialty

import gt.umg.clinica.domain.repository.SpecialtyRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(value = ["/api/doctor-specialty"])
class FindAllSpecialtyController {

    @Autowired
    private lateinit var specialtyRepository: SpecialtyRepository

    @GetMapping
    @Transactional(readOnly = true)
    fun index(): ResponseEntity<List<SpecialtyJsonDto>> {
        val result = specialtyRepository
            .findAll()
            .map {
                SpecialtyJsonDto.build {
                    this.id = it.id
                    this.name = it.name
                }
            }

        return ResponseEntity.ok(result)
    }

}
