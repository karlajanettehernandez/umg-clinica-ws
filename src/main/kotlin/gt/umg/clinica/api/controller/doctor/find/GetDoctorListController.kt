package gt.umg.clinica.api.controller.doctor.find

import gt.umg.clinica.api.controller.doctor.DoctorJsonDto
import gt.umg.clinica.domain.repository.DoctorRepository
import gt.umg.clinica.domain.shared.MapperUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class GetDoctorListController {

    @Autowired
    private lateinit var doctorRepository: DoctorRepository

    @GetMapping(value = ["/api/doctor/list"])
    fun index(): ResponseEntity<List<DoctorJsonDto>> {
        val result = doctorRepository
            .findAllActive()
            .map {
                DoctorJsonDto.build {
                    this.id = it.id
                    this.name = MapperUtils.getDoctorName(it)
                }
            }

        return ResponseEntity.ok(result)
    }

}
