package gt.umg.clinica.api.controller.documentType

import gt.umg.clinica.domain.repository.DocumentTypeRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(value = ["/api/document-type"])
class FindAllDocumentTypeController {

    @Autowired
    private lateinit var documentTypeRepository: DocumentTypeRepository

    @GetMapping
    @Transactional(readOnly = true)
    fun index(): ResponseEntity<List<DocumentTypeJsonDto>> {
        val result = documentTypeRepository
            .findAll()
            .map {
                DocumentTypeJsonDto.build {
                    this.id = it.id
                    this.name = it.name
                }
            }

        return ResponseEntity.ok(result)
    }

}
