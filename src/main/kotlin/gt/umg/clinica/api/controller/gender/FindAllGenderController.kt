package gt.umg.clinica.api.controller.gender

import gt.umg.clinica.domain.repository.GenderRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(value = ["/api/person-gender"])
class FindAllGenderController {

    @Autowired
    private lateinit var genderRepository: GenderRepository

    @GetMapping
    @Transactional(readOnly = true)
    fun index(): ResponseEntity<List<GenderJsonDto>> {
        val result = genderRepository
            .findAll()
            .map {
                GenderJsonDto.build {
                    this.id = it.id
                    this.name = it.name
                }
            }

        return ResponseEntity.ok(result)
    }

}
