package gt.umg.clinica.api.controller.patient.find

import gt.umg.clinica.api.controller.patient.PatientJsonDto
import gt.umg.clinica.domain.repository.PatientRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(value = ["/api/patient"])
class FindAllPatientsController {

    @Autowired
    private lateinit var patientRepository: PatientRepository

    @GetMapping
    @Transactional(readOnly = true)
    fun index(): ResponseEntity<List<PatientJsonDto>> {
        val result = patientRepository
            .findAllOrderByCreatedAtDesc()
            .map { row ->
                PatientJsonDto.build {
                    this.id = row.id
                    this.name = row.name
                    this.lastName = row.lastName
                    this.email = row.email
                    this.birthDate = row.birthDate
                    this.genderId = row.gender?.id
                    this.genderName = row.gender?.name
                    this.phoneNumber = row.phoneNumber
                    this.address = row.address
                    this.identificationDocumentNumber = row.identificationDocumentNumber
                    this.identificationDocumentTypeId = row.identificationDocumentType?.id
                    this.identificationDocumentTypeName = row.identificationDocumentType?.name
                    this.active = row.active
                    this.createdAt = row.createdAt
                    this.createdBy = row.createdBy
                    this.modifiedAt = row.modifiedAt
                    this.modifiedBy = row.modifiedBy
                }
            }

        return ResponseEntity.ok(result)
    }

}
