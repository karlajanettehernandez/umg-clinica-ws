package gt.umg.clinica.api.controller.medicine

import gt.umg.clinica.domain.repository.MedicineRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class GetMedicineListController {

    @Autowired
    private lateinit var medicineRepository: MedicineRepository

    @GetMapping(value = ["/api/medicine/list"])
    fun index(): ResponseEntity<List<MedicineJsonDto>> {
        val result = medicineRepository
            .findAllActiveTrue()
            .map {
                MedicineJsonDto.build {
                    this.id = it.id
                    this.name = it.name
                }
            }

        return ResponseEntity.ok(result)
    }

}
