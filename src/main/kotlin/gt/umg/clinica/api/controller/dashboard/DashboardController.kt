package gt.umg.clinica.api.controller.dashboard

import gt.umg.clinica.domain.repository.MedicalHistoryRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import java.time.LocalDateTime
import java.time.temporal.TemporalAdjusters

@RestController
class DashboardController {

    @Autowired
    private lateinit var medicalHistoryRepository: MedicalHistoryRepository

    /**
     * Obtiene la cantidad de pacientes atendidos en el dia actual
     */
    @GetMapping(value = ["/api/dashboard/patients-attended-today"])
    fun patientsAttendedToday(): ResponseEntity<Int> {
        val startDate = getCurrentDateTime()
        val endDate = getCurrentDateTime(23, 59, 59)
        val result = medicalHistoryRepository.countDistinctByCreatedAtBetween(startDate, endDate)

        return ResponseEntity.ok(result.toInt())
    }

    @GetMapping(value = ["/api/dashboard/patients-attended-in-the-month"])
    fun patientsAttendedInTheMonth(): ResponseEntity<Int> {
        val startDate = getCurrentDateTime().with(TemporalAdjusters.firstDayOfMonth())
        val endDate = getCurrentDateTime(23, 59, 59).with(TemporalAdjusters.lastDayOfMonth())
        val result = medicalHistoryRepository.countDistinctByCreatedAtBetween(startDate, endDate)

        return ResponseEntity.ok(result.toInt())
    }

    @GetMapping(value = ["/api/dashboard/patients-attended-in-the-last-month"])
    fun patientsAttendedInTheLastMonth(): ResponseEntity<Int> {
        val startDate = getCurrentDateTime()
            .minusMonths(1) // le restamos un mes para obtener la fecha inicio del mes anterior
            .with(TemporalAdjusters.firstDayOfMonth())

        val endDate = getCurrentDateTime(23, 59, 59)
            .minusMonths(1) // le restamos un mes para obtener la fecha fin del mes anterior
            .with(TemporalAdjusters.lastDayOfMonth())

        val result = medicalHistoryRepository.countDistinctByCreatedAtBetween(startDate, endDate)

        return ResponseEntity.ok(result.toInt())
    }

    private fun getCurrentDateTime(hour: Int = 0, minute: Int = 0, second: Int = 0): LocalDateTime {
        return LocalDateTime.now().withHour(hour).withMinute(minute).withSecond(second)
    }

}
