package gt.umg.clinica.api.controller.medicineInventory

import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import gt.umg.clinica.api.converters.CustomLocalDateDeserializer
import gt.umg.clinica.api.converters.CustomLocalDateSerializer
import java.math.BigDecimal
import java.time.LocalDate

class MedicineInventoryJsonDto {
    var id: Long? = null
    var purchasePrice: BigDecimal? = null
    var salePrice: BigDecimal? = null
    var quantity: Int? = null
    var quantitySale: Int? = null
    var stock: Int? = null
    var batchNumber: String? = null

    @JsonSerialize(using = CustomLocalDateSerializer::class)
    @JsonDeserialize(using = CustomLocalDateDeserializer::class)
    var expirationDate: LocalDate? = null

    var medicineId: Long? = null
    var medicineName: String? = null
    var active: Boolean? = null
    var comments: String? = null

    companion object {
        fun build(init: MedicineInventoryJsonDto.() -> Unit) = MedicineInventoryJsonDto().apply(init)
    }
}
