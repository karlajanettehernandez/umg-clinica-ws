package gt.umg.clinica.api.controller.patient

import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import gt.umg.clinica.api.converters.CustomLocalDateDeserializer
import gt.umg.clinica.api.converters.CustomLocalDateSerializer
import gt.umg.clinica.api.converters.CustomLocalDateTimeDeserializer
import gt.umg.clinica.api.converters.CustomLocalDateTimeSerializer
import gt.umg.clinica.domain.entities.Patient
import java.time.LocalDate
import java.time.LocalDateTime

class PatientJsonDto {
    var id: Long? = 0L
    var name: String? = ""
    var lastName: String? = ""
    var email: String? = ""

    @JsonSerialize(using = CustomLocalDateSerializer::class)
    @JsonDeserialize(using = CustomLocalDateDeserializer::class)
    var birthDate: LocalDate? = LocalDate.now()

    var genderId: Long? = 0L
    var genderName: String? = ""
    var phoneNumber: String? = ""
    var address: String? = ""
    var identificationDocumentNumber: String? = ""
    var identificationDocumentTypeId: Long? = 0L
    var identificationDocumentTypeName: String? = ""
    var active: Boolean? = true

    @JsonSerialize(using = CustomLocalDateTimeSerializer::class)
    @JsonDeserialize(using = CustomLocalDateTimeDeserializer::class)
    var createdAt: LocalDateTime? = LocalDateTime.now()

    var createdBy: String? = ""

    @JsonSerialize(using = CustomLocalDateTimeSerializer::class)
    @JsonDeserialize(using = CustomLocalDateTimeDeserializer::class)
    var modifiedAt: LocalDateTime? = LocalDateTime.now()

    var modifiedBy: String? = ""

    var schedule: Long? = null

    companion object {
        fun build(init: PatientJsonDto.() -> Unit) = PatientJsonDto().apply(init)
        fun fromHibernateEntity(entity: Patient) = build {
            this.id = entity.id
            this.name = entity.name
            this.lastName = entity.lastName
            this.email = entity.email
            this.birthDate = entity.birthDate
            this.genderId = entity.gender?.id
            this.genderName = entity.gender?.name
            this.phoneNumber = entity.phoneNumber
            this.address = entity.address
            this.identificationDocumentNumber = entity.identificationDocumentNumber
            this.identificationDocumentTypeId = entity.identificationDocumentType?.id
            this.identificationDocumentTypeName = entity.identificationDocumentType?.name
            this.active = entity.active
            this.createdAt = entity.createdAt
            this.createdBy = entity.createdBy
            this.modifiedAt = entity.modifiedAt
            this.modifiedBy = entity.modifiedBy
        }
    }
}
