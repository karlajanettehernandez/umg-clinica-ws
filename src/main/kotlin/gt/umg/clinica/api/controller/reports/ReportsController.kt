package gt.umg.clinica.api.controller.reports

import gt.umg.clinica.api.controller.medicalHistory.MedicalHistoryLabTestJsonDto
import gt.umg.clinica.api.controller.medicalHistory.MedicalHistoryPrescriptionJsonDto
import gt.umg.clinica.domain.exceptions.ValidationException
import gt.umg.clinica.domain.repository.MedicalHistoryLabTestRepository
import gt.umg.clinica.domain.repository.MedicalHistoryPrescriptionRepository
import gt.umg.clinica.domain.repository.MedicalHistoryRepository
import gt.umg.clinica.domain.shared.MapperUtils
import net.sf.jasperreports.engine.JREmptyDataSource
import net.sf.jasperreports.engine.JasperExportManager
import net.sf.jasperreports.engine.JasperFillManager
import net.sf.jasperreports.engine.JasperReport
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource
import net.sf.jasperreports.engine.util.JRLoader
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import java.time.format.DateTimeFormatter

@RestController
class ReportsController {

    private companion object {
        const val DATE_TIME_FORMAT = "dd/MM/yyyy HH:mm"
    }

    private val dateTimeFormatter = DateTimeFormatter.ofPattern(DATE_TIME_FORMAT)

    @Autowired
    private lateinit var medicalHistoryRepository: MedicalHistoryRepository

    @Autowired
    private lateinit var medicalHistoryPrescriptionRepository: MedicalHistoryPrescriptionRepository

    @Autowired
    private lateinit var medicalHistoryLabTestRepository: MedicalHistoryLabTestRepository

    @GetMapping(value = ["/api/reports/medical-history/{medicalHistoryId}"])
    fun getMedicalHistory(@PathVariable(value = "medicalHistoryId") medicalHistoryId: Long): ResponseEntity<ByteArray> {
        val medicalHistoryEntity = medicalHistoryRepository
            .findById(medicalHistoryId)
            .orElseThrow { ValidationException("Historial medico no existe") }

        val reportStream = javaClass.classLoader.getResourceAsStream("receta-medica.jasper")
        val jasperReport = JRLoader.loadObject(reportStream) as JasperReport

        // Lista de medicamentos recetados
        val prescriptions = medicalHistoryPrescriptionRepository
            .findByMedicalHistoryIdAndActiveTrue(medicalHistoryId)
            .map {
                MedicalHistoryPrescriptionJsonDto.build {
                    this.id = it.id
                    this.medicineId = it.medicine?.id
                    this.medicineName = it.medicine?.name
                    this.indications = it.indications
                }
            }

        // Lista de examanes
        val labTestList = medicalHistoryLabTestRepository
            .findByMedicalHistoryIdAndActiveTrue(medicalHistoryId)
            .map {
                MedicalHistoryLabTestJsonDto.build {
                    this.id = it.id
                    this.labTestId = it.labTest?.id
                    this.labTestName = it.labTest?.name
                    this.description = it.description
                }
            }

        val parameters = mapOf(
            "doctorName" to MapperUtils.getDoctorName(medicalHistoryEntity.doctor),
            "patientName" to MapperUtils.getPatientName(medicalHistoryEntity.patient),
            "patientAge" to MapperUtils.getAge(medicalHistoryEntity.patient?.birthDate),
            "issueDate" to medicalHistoryEntity.createdAt.format(dateTimeFormatter),
            "medicalHistoryDescription" to "${medicalHistoryEntity.description}",
            "medicalHistoryDiagnostic" to "${medicalHistoryEntity.diagnostic}",
            "medicalHistoryPrescriptionDs" to JRBeanCollectionDataSource(prescriptions),
            "medicalHistoryLabTestDs" to JRBeanCollectionDataSource(labTestList)
        )

        val jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, JREmptyDataSource())
        val pdfBytes = JasperExportManager.exportReportToPdf(jasperPrint)
        val headers = HttpHeaders()
        headers.set(HttpHeaders.CONTENT_DISPOSITION, "inline;filename=receta-medica.pdf")

        return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF).body(pdfBytes)
    }

}
