package gt.umg.clinica.api.controller.doctor.update

import gt.umg.clinica.api.controller.doctor.DoctorJsonDto
import gt.umg.clinica.application.doctor.DoctorRequest
import gt.umg.clinica.application.doctor.update.DoctorUpdater
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.lang.RuntimeException
import java.time.LocalDate

@RestController
@RequestMapping(value = ["/api/doctor"])
class UpdateDoctorController {

    @Autowired
    private lateinit var doctorUpdater: DoctorUpdater

    @PutMapping
    @Transactional(rollbackFor = [RuntimeException::class])
    fun index(@RequestBody data: DoctorJsonDto): ResponseEntity<Unit> {
        val request = DoctorRequest.build {
            this.id = data.id ?: 0L
            this.name = data.name ?: ""
            this.lastName = data.lastName ?: ""
            this.email = data.email ?: ""
            this.birthDate = data.birthDate ?: LocalDate.now()
            this.genderId = data.genderId ?: 0L
            this.phoneNumber = data.phoneNumber ?: ""
            this.address = data.address ?: ""
            this.activeCollegiate = data.activeCollegiate ?: ""
            this.identificationDocumentNumber = data.identificationDocumentNumber ?: ""
            this.identificationDocumentTypeId = data.identificationDocumentTypeId ?: 0L
            this.specialtyId = data.specialtyId ?: 0L
            this.active = data.active ?: false
        }

        doctorUpdater.update(request)

        return ResponseEntity.ok(Unit)
    }

}
