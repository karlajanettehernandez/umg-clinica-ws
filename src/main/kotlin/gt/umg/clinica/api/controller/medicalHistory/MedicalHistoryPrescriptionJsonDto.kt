package gt.umg.clinica.api.controller.medicalHistory

class MedicalHistoryPrescriptionJsonDto {
    var id: Long? = null
    var medicineId: Long? = 0L
    var medicineName: String? = ""
    var indications: String? = ""

    companion object {
        fun build(init: MedicalHistoryPrescriptionJsonDto.() -> Unit) = MedicalHistoryPrescriptionJsonDto().apply(init)
    }
}
