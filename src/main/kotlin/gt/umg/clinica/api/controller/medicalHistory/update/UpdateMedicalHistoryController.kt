package gt.umg.clinica.api.controller.medicalHistory.update

import gt.umg.clinica.api.controller.medicalHistory.MedicalHistoryJsonDto
import gt.umg.clinica.application.medicalHistory.create.MedicalHistoryInventoryReferenceCreator
import gt.umg.clinica.application.medicalHistory.create.MedicalHistoryLabTestCreator
import gt.umg.clinica.application.medicalHistory.create.MedicalHistoryPrescriptionCreator
import gt.umg.clinica.domain.exceptions.ValidationException
import gt.umg.clinica.domain.repository.DoctorRepository
import gt.umg.clinica.domain.repository.MedicalHistoryRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class UpdateMedicalHistoryController {

    @Autowired
    private lateinit var medicalHistoryRepository: MedicalHistoryRepository

    @Autowired
    private lateinit var doctorRepository: DoctorRepository

    @Autowired
    private lateinit var medicalHistoryLabTestCreator: MedicalHistoryLabTestCreator

    @Autowired
    private lateinit var medicalHistoryPrescriptionCreator: MedicalHistoryPrescriptionCreator

    @Autowired
    private lateinit var medicalHistoryInventoryReferenceCreator: MedicalHistoryInventoryReferenceCreator

    @PutMapping(value = ["/api/medical-history"])
    fun index(@RequestBody data: MedicalHistoryJsonDto): ResponseEntity<Unit> {
        val id = data.id ?: 0L

        val doctorEntity = doctorRepository
            .findById(data.doctorId ?: 0L)
            .orElseThrow { ValidationException("Medico no existe") }

        val entity = medicalHistoryRepository
            .findById(id)
            .orElseThrow { ValidationException("Historia medica no existe") }

        entity.apply {
            this.doctor = doctorEntity
            this.description = data.description
            this.diagnostic = data.diagnostic
        }

        medicalHistoryRepository.save(entity)

        medicalHistoryLabTestCreator.create(id, data.labTestList ?: listOf())
        medicalHistoryPrescriptionCreator.create(id, data.prescriptionList ?: listOf())
        medicalHistoryInventoryReferenceCreator.create(id, data.inventoryReferenceList ?: listOf())

        return ResponseEntity.ok(Unit)
    }

}
