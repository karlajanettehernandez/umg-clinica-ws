package gt.umg.clinica.api.controller.medicine

class MedicineJsonDto {
    var id: Long? = null
    var name: String? = null

    companion object {
        fun build(init: MedicineJsonDto.() -> Unit) = MedicineJsonDto().apply(init)
    }
}
