package gt.umg.clinica.api.controller.medicalHistory.find

import gt.umg.clinica.api.controller.medicalHistory.MedicalHistoryInventoryReferenceJsonDto
import gt.umg.clinica.api.controller.medicalHistory.MedicalHistoryJsonDto
import gt.umg.clinica.api.controller.medicalHistory.MedicalHistoryLabTestJsonDto
import gt.umg.clinica.api.controller.medicalHistory.MedicalHistoryPrescriptionJsonDto
import gt.umg.clinica.domain.repository.MedicalHistoryInventoryReferenceRepository
import gt.umg.clinica.domain.repository.MedicalHistoryRepository
import gt.umg.clinica.domain.repository.MedicalHistoryLabTestRepository
import gt.umg.clinica.domain.repository.MedicalHistoryPrescriptionRepository
import gt.umg.clinica.domain.shared.MapperUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

@RestController
class FindMedicalHistoryController {

    @Autowired
    private lateinit var medicalHistoryRepository: MedicalHistoryRepository

    @Autowired
    private lateinit var medicalHistoryLabTestRepository: MedicalHistoryLabTestRepository

    @Autowired
    private lateinit var medicalHistoryPrescriptionRepository: MedicalHistoryPrescriptionRepository

    @Autowired
    private lateinit var medicalHistoryInventoryReferenceRepository: MedicalHistoryInventoryReferenceRepository

    @GetMapping(value = ["/api/patient/{patientId}/medical-history"])
    fun findByPatientId(
        @PathVariable(value = "patientId") patientId: Long
    ): ResponseEntity<List<MedicalHistoryJsonDto>> {
        val result = medicalHistoryRepository
            .findByPatientIdAndActiveTrue(patientId)
            .map { history ->
                MedicalHistoryJsonDto.build {
                    this.id = history.id
                    this.doctorId = history.doctor?.id
                    this.doctorName = MapperUtils.getDoctorName(history.doctor)
                    this.patientId = history.patient?.id
                    this.patientName = MapperUtils.getPatientName(history.patient)
                    this.description = history.description
                    this.diagnostic = history.diagnostic
                    this.active = history.active
                    this.createdAt = history.createdAt
                    this.createdBy = history.createdBy
                    this.modifiedAt = history.modifiedAt
                    this.modifiedBy = history.modifiedBy
                }
            }

        return ResponseEntity.ok(result)
    }

    @GetMapping(value = ["/api/medical-history/{medicalHistoryId}/lab-test"])
    fun findLabTestByMedicalHistoryId(
        @PathVariable(value = "medicalHistoryId") medicalHistoryId: Long
    ): ResponseEntity<List<MedicalHistoryLabTestJsonDto>> {
        val result = medicalHistoryLabTestRepository
            .findByMedicalHistoryIdAndActiveTrue(medicalHistoryId)
            .map {
                MedicalHistoryLabTestJsonDto.build {
                    this.id = it.id
                    this.labTestId = it.labTest?.id
                    this.labTestName = it.labTest?.name
                    this.description = it.description
                }
            }

        return ResponseEntity.ok(result)
    }

    @GetMapping(value = ["/api/medical-history/{medicalHistoryId}/prescriptions"])
    fun findPrescriptionsByMedicalHistoryId(
        @PathVariable(value = "medicalHistoryId") medicalHistoryId: Long
    ): ResponseEntity<List<MedicalHistoryPrescriptionJsonDto>> {
        val result = medicalHistoryPrescriptionRepository
            .findByMedicalHistoryIdAndActiveTrue(medicalHistoryId)
            .map {
                MedicalHistoryPrescriptionJsonDto.build {
                    this.id = it.id
                    this.medicineId = it.medicine?.id
                    this.medicineName = it.medicine?.name
                    this.indications = it.indications
                }
            }

        return ResponseEntity.ok(result)
    }

    @GetMapping(value = ["/api/medical-history/{medicalHistoryId}/inventory-reference"])
    fun findInventoryReference(
        @PathVariable(value = "medicalHistoryId") medicalHistoryId: Long
    ): ResponseEntity<List<MedicalHistoryInventoryReferenceJsonDto>> {
        val result = medicalHistoryInventoryReferenceRepository
            .findByMedicalHistoryIdAndActiveTrue(medicalHistoryId)
            .map {
                MedicalHistoryInventoryReferenceJsonDto.build {
                    this.id = it.id
                    this.medicineInventoryId = it.medicineInventory?.id
                    this.medicineName = it.medicineInventory?.medicine?.name
                    this.quantity = it.quantity
                }
            }

        return ResponseEntity.ok(result)
    }

}
