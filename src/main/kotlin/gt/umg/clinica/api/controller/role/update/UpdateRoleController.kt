package gt.umg.clinica.api.controller.role.update

import gt.umg.clinica.api.controller.role.RoleJson
import gt.umg.clinica.application.role.RoleMenuRequest
import gt.umg.clinica.application.role.RoleRequest
import gt.umg.clinica.application.role.update.RoleUpdater
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class UpdateRoleController {

    @Autowired
    private lateinit var roleUpdater: RoleUpdater

    @PutMapping(value = ["/api/role"])
    @Transactional
    fun index(@RequestBody data: RoleJson): ResponseEntity<Unit> {
        val roleRequest = RoleRequest.build {
            this.id = data.id ?: 0L
            this.name = data.name ?: ""
            this.code = data.code ?: ""
            this.active = data.active ?: false
            this.menuList = data.menuList?.map {
                RoleMenuRequest.build {
                    this.menuId = it.menuId ?: 0L
                    this.permissions = it.permissions ?: listOf()
                }
            } ?: listOf()
        }

        roleUpdater.update(roleRequest)

        return ResponseEntity.ok(Unit)
    }

}
