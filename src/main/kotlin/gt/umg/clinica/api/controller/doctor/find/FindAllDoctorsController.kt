package gt.umg.clinica.api.controller.doctor.find

import gt.umg.clinica.api.controller.doctor.DoctorJsonDto
import gt.umg.clinica.domain.repository.DoctorRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class FindAllDoctorsController {

    @Autowired
    private lateinit var doctorRepository: DoctorRepository

    @GetMapping(value = ["/api/doctor"])
    @Transactional(readOnly = true)
    fun index(): ResponseEntity<List<DoctorJsonDto>> {
        val result = doctorRepository
            .findAllOrderByCreatedAtDesc()
            .map { row ->
                DoctorJsonDto.build {
                    this.id = row.id
                    this.name = row.name
                    this.lastName = row.lastName
                    this.email = row.email
                    this.birthDate = row.birthDate
                    this.genderId = row.gender?.id
                    this.genderName = row.gender?.name
                    this.phoneNumber = row.phoneNumber
                    this.address = row.address
                    this.activeCollegiate = row.activeCollegiate
                    this.identificationDocumentNumber = row.identificationDocumentNumber
                    this.identificationDocumentTypeId = row.identificationDocumentType?.id
                    this.identificationDocumentTypeName = row.identificationDocumentType?.name
                    this.specialtyId = row.specialty?.id
                    this.specialtyName = row.specialty?.name
                    this.active = row.active
                    this.createdAt = row.createdAt
                    this.createdBy = row.createdBy
                    this.modifiedAt = row.modifiedAt
                    this.modifiedBy = row.modifiedBy
                }
            }

        return ResponseEntity.ok(result)
    }

}
