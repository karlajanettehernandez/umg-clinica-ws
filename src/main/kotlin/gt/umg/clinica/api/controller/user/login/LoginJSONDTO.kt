package gt.umg.clinica.api.controller.user.login

class LoginJSONDTO {
    var email: String? = null
    var password: String? = null
}
