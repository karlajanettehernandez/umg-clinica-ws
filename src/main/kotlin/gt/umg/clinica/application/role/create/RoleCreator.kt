package gt.umg.clinica.application.role.create

import gt.umg.clinica.application.role.RoleRequest
import gt.umg.clinica.domain.exceptions.ValidationException
import gt.umg.clinica.domain.validators.LongValueObject
import gt.umg.clinica.domain.validators.StringValueObject
import gt.umg.clinica.domain.entities.Role
import gt.umg.clinica.domain.entities.RoleMenu
import gt.umg.clinica.domain.repository.ApplicationMenuRepository
import gt.umg.clinica.domain.repository.RoleRepository
import gt.umg.clinica.domain.repository.RoleMenuRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class RoleCreator @Autowired constructor(
    private val roleRepository: RoleRepository,
    private val roleMenuRepository: RoleMenuRepository,
    private val applicationMenuRepository: ApplicationMenuRepository
) {

    fun create(request: RoleRequest): Long {
        val roleCode = StringValueObject.valueOf(request.code)
        val roleName = StringValueObject.valueOf(request.name)

        roleCode.validate()
        roleName.validate()

        val otherRole = roleRepository.findByCode(roleCode.value)

        // 1. Validamos si existe un rol con el mismo codigo lanzamos un error
        if (otherRole != null) {
            throw ValidationException("Ya existe un rol con el codigo ${roleCode.value}")
        }

        val roleEntity = Role.build {
            this.code = roleCode.value
            this.name = roleName.value
        }

        // 2. Guarda el base de datos el nuevo rol
        roleRepository.save(roleEntity)

        val roleId = LongValueObject.valueOf(roleEntity.id)
        roleId.validate()

        // 3. Asocia al nuevo rol los menus a los que tendra acceso
        val menus = request
            .menuList
            .map { menu ->
                val applicationMenuEntity = applicationMenuRepository
                    .findById(menu.menuId)
                    .orElseThrow { ValidationException("La entidad Menu no existe") }

                return@map RoleMenu.build {
                    this.applicationMenu = applicationMenuEntity
                    this.role = roleEntity
                    this.permissions = menu.permissions
                }
            }
            .toList()

        roleMenuRepository.saveAll(menus)

        return roleId.value
    }

}
