package gt.umg.clinica.application.role

class RoleMenuRequest {
    var menuId: Long = 0
    var permissions: List<String> = listOf()

    companion object {
        fun build(init: RoleMenuRequest.() -> Unit) = RoleMenuRequest().apply(init)
    }
}
