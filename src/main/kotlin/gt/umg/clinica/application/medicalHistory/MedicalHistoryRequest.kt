package gt.umg.clinica.application.medicalHistory

import java.time.LocalDateTime

class MedicalHistoryRequest {
    var id: Long? = null
    var doctorId: Long? = null
    var doctorName: String? = null
    var patientId: Long? = null
    var patientName: String? = null
    var description: String? = null
    var diagnostic: String? = null
    var active: Boolean? = null
    var createdAt: LocalDateTime? = null

    companion object {
        fun build(init: MedicalHistoryRequest.() -> Unit) = MedicalHistoryRequest().apply(init)
    }
}