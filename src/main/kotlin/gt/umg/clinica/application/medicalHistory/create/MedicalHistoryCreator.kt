package gt.umg.clinica.application.medicalHistory.create

import gt.umg.clinica.application.medicalHistory.MedicalHistoryRequest
import gt.umg.clinica.domain.exceptions.ValidationException
import gt.umg.clinica.domain.validators.LongValueObject
import gt.umg.clinica.domain.validators.StringValueObject
import gt.umg.clinica.domain.entities.MedicalHistory
import gt.umg.clinica.domain.repository.DoctorRepository
import gt.umg.clinica.domain.repository.MedicalHistoryRepository
import gt.umg.clinica.domain.repository.PatientRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.time.LocalDateTime

@Component
class MedicalHistoryCreator @Autowired constructor(
    private val medicalHistoryRepository: MedicalHistoryRepository,
    private val patientRepository: PatientRepository,
    private val doctorRepository: DoctorRepository
) {

    fun create(request: MedicalHistoryRequest): Long {
        val patientId = LongValueObject.valueOf(request.patientId)
        val doctorId = LongValueObject.valueOf(request.doctorId)
        val description = StringValueObject.valueOf(request.description)
        val diagnostic = StringValueObject.valueOf(request.diagnostic)

        patientId.validate()
        doctorId.validate()
        description.validate()
        diagnostic.validate()

        val patientEntity = patientRepository
            .findById(patientId.value)
            .orElseThrow { ValidationException("Paciente no existe") }

        val doctorEntity = doctorRepository
            .findById(doctorId.value)
            .orElseThrow { ValidationException("Medico no existe") }


        val entity = MedicalHistory.build {
            this.patient = patientEntity
            this.doctor = doctorEntity
            this.description = description.value
            this.diagnostic = diagnostic.value
            this.createdAt = LocalDateTime.now()
        }

        medicalHistoryRepository.save(entity)

        val id = LongValueObject.valueOf(entity.id)

        id.validate()

        return id.value
    }

}
