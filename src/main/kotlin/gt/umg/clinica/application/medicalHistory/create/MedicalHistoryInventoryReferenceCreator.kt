package gt.umg.clinica.application.medicalHistory.create

import gt.umg.clinica.api.controller.medicalHistory.MedicalHistoryInventoryReferenceJsonDto
import gt.umg.clinica.domain.entities.MedicalHistoryInventoryReference
import gt.umg.clinica.domain.entities.MedicineInventory
import gt.umg.clinica.domain.exceptions.ValidationException
import gt.umg.clinica.domain.repository.MedicalHistoryInventoryReferenceRepository
import gt.umg.clinica.domain.repository.MedicalHistoryRepository
import gt.umg.clinica.domain.repository.MedicineInventoryRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class MedicalHistoryInventoryReferenceCreator @Autowired constructor(
    private val medicalHistoryRepository: MedicalHistoryRepository,
    private val medicineInventoryRepository: MedicineInventoryRepository,
    private val medicalHistoryInventoryReferenceRepository: MedicalHistoryInventoryReferenceRepository
) {

    fun create(medicalHistoryId: Long, inventoryReferenceList: List<MedicalHistoryInventoryReferenceJsonDto>) {
        deleteExistingEntities(medicalHistoryId)

        val medicalHistoryEntity = medicalHistoryRepository
            .findById(medicalHistoryId)
            .orElseThrow { ValidationException("El historial medico $medicalHistoryId no existe") }

        val inventoryToUpdate = mutableListOf<MedicineInventory>()

        val references = inventoryReferenceList
            .map {
                val medicineInventoryEntity = medicineInventoryRepository
                    .findById(it.medicineInventoryId ?: 0L)
                    .orElseThrow { ValidationException("El inventario a actualizar no existe") }

                val quantitySale = (medicineInventoryEntity.quantitySale ?: 0) + (it.quantity ?: 0)
                val stock = (medicineInventoryEntity.quantity ?: 0) - quantitySale

                medicineInventoryEntity.quantitySale = quantitySale
                medicineInventoryEntity.stock = stock

                inventoryToUpdate.add(medicineInventoryEntity)

                return@map MedicalHistoryInventoryReference.build {
                    this.medicalHistory = medicalHistoryEntity
                    this.medicineInventory = medicineInventoryEntity
                    this.quantity = it.quantity
                }
            }

        medicineInventoryRepository.saveAll(inventoryToUpdate)
        medicalHistoryInventoryReferenceRepository.saveAll(references)
    }

    private fun deleteExistingEntities(medicalHistoryId: Long) {
        val existingEntities = medicalHistoryInventoryReferenceRepository.findByMedicalHistoryId(medicalHistoryId)

        if (existingEntities.isEmpty()) return

        val inventoryToUpdate = mutableListOf<MedicineInventory>()

        existingEntities.forEach {
            val medicineInventoryEntity = medicineInventoryRepository
                .findById(it.medicineInventory?.id ?: 0L)
                .orElseThrow { ValidationException("El inventario a actualizar no existe") }

            val quantitySale = (medicineInventoryEntity.quantitySale ?: 0) - (it.quantity ?: 0)
            val stock = (medicineInventoryEntity.quantity ?: 0) - quantitySale

            medicineInventoryEntity.quantitySale = quantitySale
            medicineInventoryEntity.stock = stock

            inventoryToUpdate.add(medicineInventoryEntity)
        }

        medicineInventoryRepository.saveAll(inventoryToUpdate)
        medicalHistoryInventoryReferenceRepository.deleteAll(existingEntities)
    }

}
