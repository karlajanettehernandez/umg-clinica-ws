package gt.umg.clinica.application.medicalHistory.create

import gt.umg.clinica.api.controller.medicalHistory.MedicalHistoryLabTestJsonDto
import gt.umg.clinica.domain.exceptions.ValidationException
import gt.umg.clinica.domain.entities.MedicalHistoryLabTest
import gt.umg.clinica.domain.repository.LabTestRepository
import gt.umg.clinica.domain.repository.MedicalHistoryRepository
import gt.umg.clinica.domain.repository.MedicalHistoryLabTestRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class MedicalHistoryLabTestCreator @Autowired constructor(
    private val medicalHistoryLabTestRepository: MedicalHistoryLabTestRepository,
    private val medicalHistoryRepository: MedicalHistoryRepository,
    private val labTestRepository: LabTestRepository
) {

    fun create(medicalHistoryId: Long, labTestList: List<MedicalHistoryLabTestJsonDto>) {
        deleteExistingEntities(medicalHistoryId)

        val medicalHistoryEntity = medicalHistoryRepository
            .findById(medicalHistoryId)
            .orElseThrow { ValidationException("El historial medico $medicalHistoryId no existe") }

        val entities = labTestList
            .map {
                val labTestEntity = labTestRepository
                    .findById(it.labTestId ?: 0L)
                    .orElseThrow { throw ValidationException("El test de laboratorio ${it.labTestId} no existe") }

                return@map MedicalHistoryLabTest.build {
                    this.medicalHistory = medicalHistoryEntity
                    this.labTest = labTestEntity
                    this.description = it.description
                }
            }
            .toList()

        medicalHistoryLabTestRepository.saveAll(entities)
    }

    private fun deleteExistingEntities(medicalHistoryId: Long) {
        val existingEntities = medicalHistoryLabTestRepository.findByMedicalHistoryId(medicalHistoryId)

        if (existingEntities.isNotEmpty()) {
            medicalHistoryLabTestRepository.deleteAll(existingEntities)
        }
    }

}
