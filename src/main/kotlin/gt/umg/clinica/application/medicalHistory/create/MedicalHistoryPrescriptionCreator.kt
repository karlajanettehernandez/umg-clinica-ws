package gt.umg.clinica.application.medicalHistory.create

import gt.umg.clinica.api.controller.medicalHistory.MedicalHistoryPrescriptionJsonDto
import gt.umg.clinica.domain.exceptions.ValidationException
import gt.umg.clinica.domain.entities.MedicalHistoryPrescription
import gt.umg.clinica.domain.repository.MedicalHistoryRepository
import gt.umg.clinica.domain.repository.MedicalHistoryPrescriptionRepository
import gt.umg.clinica.domain.repository.MedicineRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class MedicalHistoryPrescriptionCreator @Autowired constructor(
    private val medicalHistoryRepository: MedicalHistoryRepository,
    private val medicineRepository: MedicineRepository,
    private val medicalHistoryPrescriptionRepository: MedicalHistoryPrescriptionRepository
) {

    fun create(medicalHistoryId: Long, prescriptionList: List<MedicalHistoryPrescriptionJsonDto>) {
        deleteExistingEntities(medicalHistoryId)

        val medicalHistoryEntity = medicalHistoryRepository
            .findById(medicalHistoryId)
            .orElseThrow { ValidationException("El historial medico $medicalHistoryId no existe") }

        val entities = prescriptionList
            .map {
                val medicineEntity = medicineRepository
                    .findById(it.medicineId ?: 0L)
                    .orElseThrow { ValidationException("El medicamento ${it.medicineId} no existe") }

                return@map MedicalHistoryPrescription.build {
                    this.medicalHistory = medicalHistoryEntity
                    this.medicine = medicineEntity
                    this.indications = it.indications
                }
            }
            .toList()

        medicalHistoryPrescriptionRepository.saveAll(entities)
    }

    private fun deleteExistingEntities(medicalHistoryId: Long) {
        val existingEntities = medicalHistoryPrescriptionRepository.findByMedicalHistoryId(medicalHistoryId)

        if (existingEntities.isNotEmpty()) {
            medicalHistoryPrescriptionRepository.deleteAll(existingEntities)
        }
    }

}
