package gt.umg.clinica.application.patient.update

import gt.umg.clinica.application.patient.PatientRequest
import gt.umg.clinica.domain.exceptions.ValidationException
import gt.umg.clinica.domain.validators.EmailValueObject
import gt.umg.clinica.domain.validators.LongValueObject
import gt.umg.clinica.domain.validators.StringValueObject
import gt.umg.clinica.domain.repository.GenderRepository
import gt.umg.clinica.domain.repository.PatientRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class PatientUpdater @Autowired constructor(
    private val patientRepository: PatientRepository,
    private val genderRepository: GenderRepository
) {
    fun update(request: PatientRequest) {
        val id = LongValueObject.valueOf(request.id)
        val name = StringValueObject.valueOf(request.name)
        val lastName = StringValueObject.valueOf(request.lastName)
        val email = EmailValueObject.valueOf(request.email)
        val genderId = LongValueObject.valueOf(request.genderId)
        val phoneNumber = StringValueObject.valueOf(request.phoneNumber)
        val address = StringValueObject.valueOf(request.address)
        val identificationDocumentNumber = StringValueObject.valueOf(request.identificationDocumentNumber)

        name.validate()
        lastName.validate()
        phoneNumber.validate()
        address.validate()
        identificationDocumentNumber.validate()
        genderId.validate()

        val entityToUpdate = patientRepository
            .findById(id.value)
            .orElseThrow { throw ValidationException("Entidad $id no existe") }

        val exists = patientRepository
            .existsByIdentificationDocumentNumberAndIdNotEquals(identificationDocumentNumber.value, id.value)

        if (exists) {
            val errorMessage = "Ya existe otro paciente con el numero de identificacion $identificationDocumentNumber"
            throw ValidationException(errorMessage)
        }

        val genderEntity = genderRepository
            .findById(genderId.value)
            .orElseThrow { ValidationException("La entidad Gender no existe") }

        entityToUpdate.apply {
            this.name = name.value
            this.lastName = lastName.value
            this.email = email.value
            this.birthDate = request.birthDate
            this.phoneNumber = phoneNumber.value
            this.address = address.value
            this.identificationDocumentNumber = identificationDocumentNumber.value
            this.gender = genderEntity
            this.active = request.active
        }

        patientRepository.save(entityToUpdate)
    }
}
