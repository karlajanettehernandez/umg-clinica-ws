package gt.umg.clinica.application.patient.delete

import gt.umg.clinica.domain.exceptions.ValidationException
import gt.umg.clinica.domain.validators.LongValueObject
import gt.umg.clinica.domain.repository.PatientRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class PatientDeleter @Autowired constructor(private val patientRepository: PatientRepository) {
    fun delete(id: Long) {
        val patientId = LongValueObject.valueOf(id)
        patientId.validate()

        val entity = patientRepository
            .findById(patientId.value)
            .orElseThrow { ValidationException("El paciente no existe") }

        entity.active = false

        patientRepository.save(entity)
    }
}
