package gt.umg.clinica.application.patient

import java.time.LocalDate

class PatientRequest {
    var id: Long = 0L
    var name: String = ""
    var lastName: String = ""
    var email: String = ""
    var birthDate: LocalDate = LocalDate.now()
    var genderId: Long = 0L
    var phoneNumber: String = ""
    var address: String = ""
    var identificationDocumentNumber: String = ""
    var identificationDocumentTypeId: Long = 0L
    var active: Boolean = true
    var schedule: Long = 0L

    companion object {
        fun build(init: PatientRequest.() -> Unit) = PatientRequest().apply(init)
    }
}
