package gt.umg.clinica.application.patient.create

import gt.umg.clinica.application.patient.PatientRequest
import gt.umg.clinica.domain.exceptions.ValidationException
import gt.umg.clinica.domain.validators.EmailValueObject
import gt.umg.clinica.domain.validators.LongValueObject
import gt.umg.clinica.domain.validators.StringValueObject
import gt.umg.clinica.domain.entities.Patient
import gt.umg.clinica.domain.repository.DocumentTypeRepository
import gt.umg.clinica.domain.repository.GenderRepository
import gt.umg.clinica.domain.repository.MedicalScheduleRepository
import gt.umg.clinica.domain.repository.PatientRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class PatientCreator @Autowired constructor(
    private val patientRepository: PatientRepository,
    private val medicalScheduleRepository: MedicalScheduleRepository,
    private val genderRepository: GenderRepository,
    private val documentTypeRepository: DocumentTypeRepository,
) {
    fun create(request: PatientRequest): Long {
        val name = StringValueObject.valueOf(request.name)
        val lastName = StringValueObject.valueOf(request.lastName)
        val email = EmailValueObject.valueOf(request.email)
        val genderId = LongValueObject.valueOf(request.genderId)
        val phoneNumber = StringValueObject.valueOf(request.phoneNumber)
        val address = StringValueObject.valueOf(request.address)
        val identificationDocumentNumber = StringValueObject.valueOf(request.identificationDocumentNumber)

        // Valida los campos requeridos
        name.validate()
        lastName.validate()
        phoneNumber.validate()
        address.validate()
        identificationDocumentNumber.validate()
        genderId.validate()

        // Valida que no exista otro paciente con el mismo numero de documento
        val exists = patientRepository.existsByIdentificationDocumentNumber(identificationDocumentNumber.value)

        if (exists) {
            val errorMessage = "Ya existe otro paciente con el numero de identificacion $identificationDocumentNumber"
            throw ValidationException(errorMessage)
        }

        // Crea la entidad
        val genderEntity = genderRepository
            .findById(genderId.value)
            .orElseThrow { ValidationException("La entidad Gender no existe") }

        val documentTypeEntity = documentTypeRepository
            .findByCode("DPI")
            ?: throw ValidationException("La entidad DocumentType no existe")

        val entity = Patient.build {
            this.name = name.value
            this.lastName = lastName.value
            this.email = email.value
            this.birthDate = request.birthDate
            this.phoneNumber = phoneNumber.value
            this.address = address.value
            this.identificationDocumentNumber = identificationDocumentNumber.value
            this.gender = genderEntity
            this.identificationDocumentType = documentTypeEntity
        }

        patientRepository.save(entity)

        val generatedId = LongValueObject.valueOf(entity.id)
        generatedId.validate()

        // Valida si el paciente se creo a partir de una cita medica
        validateSchedule(request.schedule, entity)

        return generatedId.value
    }

    private fun validateSchedule(scheduleId: Long, patient: Patient) {
        if (scheduleId <= 0L) {
            return
        }

        val scheduleEntity = medicalScheduleRepository
            .findById(scheduleId)
            .orElseThrow { ValidationException("Entidad MedicalSchedule no existe") }

        scheduleEntity.patient = patient
        medicalScheduleRepository.save(scheduleEntity)

    }
}
