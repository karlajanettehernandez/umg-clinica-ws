package gt.umg.clinica.application.doctor.delete

import gt.umg.clinica.domain.exceptions.ValidationException
import gt.umg.clinica.domain.validators.LongValueObject
import gt.umg.clinica.domain.repository.DoctorRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class DoctorDeleter @Autowired constructor(private val doctorRepository: DoctorRepository) {
    fun delete(id: Long) {
        val entityId = LongValueObject.valueOf(id)
        entityId.validate()

        val entity = doctorRepository
            .findById(entityId.value)
            .orElseThrow { ValidationException("Entidad a eliminar no existe") }

        entity.active = false

        doctorRepository.save(entity)
    }
}
