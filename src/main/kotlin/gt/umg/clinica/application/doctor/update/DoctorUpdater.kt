package gt.umg.clinica.application.doctor.update

import gt.umg.clinica.application.doctor.DoctorRequest
import gt.umg.clinica.domain.exceptions.ValidationException
import gt.umg.clinica.domain.validators.EmailValueObject
import gt.umg.clinica.domain.validators.LongValueObject
import gt.umg.clinica.domain.validators.StringValueObject
import gt.umg.clinica.domain.repository.DoctorRepository
import gt.umg.clinica.domain.repository.GenderRepository
import gt.umg.clinica.domain.repository.SpecialtyRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class DoctorUpdater @Autowired constructor(
    private val doctorRepository: DoctorRepository,
    private val genderRepository: GenderRepository,
    private val specialtyRepository: SpecialtyRepository
) {
    fun update(request: DoctorRequest) {
        val id = LongValueObject.valueOf(request.id)
        val name = StringValueObject.valueOf(request.name)
        val lastName = StringValueObject.valueOf(request.lastName)
        val email = EmailValueObject.valueOf(request.email)
        val genderId = LongValueObject.valueOf(request.genderId)
        val phoneNumber = StringValueObject.valueOf(request.phoneNumber)
        val address = StringValueObject.valueOf(request.address)
        val activeCollegiate = StringValueObject.valueOf(request.activeCollegiate)
        val identificationDocumentNumber = StringValueObject.valueOf(request.identificationDocumentNumber)
        val specialtyId = LongValueObject.valueOf(request.specialtyId)

        // Valida los campos requeridos
        name.validate()
        lastName.validate()
        email.validate()
        genderId.validate()
        phoneNumber.validate()
        address.validate()
        activeCollegiate.validate()
        identificationDocumentNumber.validate()
        specialtyId.validate()

        val entityToUpdate = doctorRepository
            .findById(id.value)
            .orElseThrow { ValidationException("Entidad $id no existe") }

        // Valida que no exista otro doctor con el mismo numero de documento
        val existsOther = doctorRepository
            .existsByIdentificationDocumentNumberAndIdNotEquals(identificationDocumentNumber.value, id.value)

        if (existsOther) {
            val errorMessage = "Ya existe otro doctor con numero de identificacion $identificationDocumentNumber"
            throw ValidationException(errorMessage)
        }

        // valida que no exista otro doctor con el mismo colegiado activo
        val existsOtherCollegiate = doctorRepository
            .existsByActiveCollegiateAndIdNotEquals(activeCollegiate.value, id.value)

        if (existsOtherCollegiate) {
            val errorMessage = "Ya existe otro doctor con numero de colegiado $activeCollegiate"
            throw ValidationException(errorMessage)
        }

        // actualiza la entidad a partir de los datos de la solicitud
        val genderEntity = genderRepository
            .findById(genderId.value)
            .orElseThrow { ValidationException("La entidad Gender no existe") }

        val specialtyEntity = specialtyRepository
            .findById(specialtyId.value)
            .orElseThrow { ValidationException("La entidad Specialty no existe") }

        entityToUpdate.apply {
            this.name = name.value
            this.lastName = lastName.value
            this.email = email.value
            this.birthDate = request.birthDate
            this.phoneNumber = phoneNumber.value
            this.address = address.value
            this.activeCollegiate = activeCollegiate.value
            this.identificationDocumentNumber = identificationDocumentNumber.value
            this.gender = genderEntity
            this.specialty = specialtyEntity
            this.active = request.active
        }

        doctorRepository.save(entityToUpdate)
    }
}
