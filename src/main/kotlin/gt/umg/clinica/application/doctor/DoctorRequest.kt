package gt.umg.clinica.application.doctor

import java.time.LocalDate

class DoctorRequest {
    var id: Long = 0L
    var name: String = ""
    var lastName: String = ""
    var email: String = ""
    var birthDate: LocalDate = LocalDate.now()
    var genderId: Long = 0L
    var phoneNumber: String = ""
    var address: String = ""
    var activeCollegiate: String = ""
    var identificationDocumentNumber: String = ""
    var identificationDocumentTypeId: Long = 0L
    var specialtyId: Long = 0L
    var active: Boolean = false

    companion object {
        fun build(init: DoctorRequest.() -> Unit) = DoctorRequest().apply(init)
    }
}
