package gt.umg.clinica.application.doctor.create

import gt.umg.clinica.application.doctor.DoctorRequest
import gt.umg.clinica.domain.exceptions.ValidationException
import gt.umg.clinica.domain.validators.EmailValueObject
import gt.umg.clinica.domain.validators.LongValueObject
import gt.umg.clinica.domain.validators.StringValueObject
import gt.umg.clinica.domain.entities.Doctor
import gt.umg.clinica.domain.repository.DoctorRepository
import gt.umg.clinica.domain.repository.DocumentTypeRepository
import gt.umg.clinica.domain.repository.GenderRepository
import gt.umg.clinica.domain.repository.SpecialtyRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class DoctorCreator @Autowired constructor(
    private val doctorRepository: DoctorRepository,
    private val genderRepository: GenderRepository,
    private val documentTypeRepository: DocumentTypeRepository,
    private val specialtyRepository: SpecialtyRepository
) {
    fun create(request: DoctorRequest): Long {

        val name = StringValueObject.valueOf(request.name)
        val lastName = StringValueObject.valueOf(request.lastName)
        val email = EmailValueObject.valueOf(request.email)
        val genderId = LongValueObject.valueOf(request.genderId)
        val phoneNumber = StringValueObject.valueOf(request.phoneNumber)
        val address = StringValueObject.valueOf(request.address)
        val activeCollegiate = StringValueObject.valueOf(request.activeCollegiate)
        val identificationDocumentNumber = StringValueObject.valueOf(request.identificationDocumentNumber)
        val specialtyId = LongValueObject.valueOf(request.specialtyId)

        // Valida los campos requeridos
        name.validate()
        lastName.validate()
        email.validate()
        genderId.validate()
        phoneNumber.validate()
        address.validate()
        activeCollegiate.validate()
        identificationDocumentNumber.validate()
        specialtyId.validate()


        // Valida que no exista otro doctor con el mismo numero de documento
        val existsOther = doctorRepository.existsByIdentificationDocumentNumber(identificationDocumentNumber.value)

        if (existsOther) {
            val errorMessage =
                "Ya existe otro doctor con numero de identificacion ${request.identificationDocumentNumber}"
            throw ValidationException(errorMessage)
        }

        // valida que no exista otro doctor con el mismo colegiado activo
        val existsOtherCollegiate = doctorRepository.existsByActiveCollegiate(activeCollegiate.value)

        if (existsOtherCollegiate) {
            val errorMessage =
                "Ya existe otro doctor con numero de colegiado ${request.activeCollegiate}"
            throw ValidationException(errorMessage)
        }

        // Crea la entidad a partir de los datos de la solicitud
        val genderEntity = genderRepository
            .findById(genderId.value)
            .orElseThrow { ValidationException("La entidad Gender no existe") }

        // Obtiene el tipo de documento DPI
        val documentTypeEntity = documentTypeRepository
            .findByCode("DPI")
            ?: throw ValidationException("La entidad DocumentType no existe")

        val specialtyEntity = specialtyRepository
            .findById(specialtyId.value)
            .orElseThrow { ValidationException("La entidad Specialty no existe") }

        val entity = Doctor.build {
            this.name = name.value
            this.lastName = lastName.value
            this.email = email.value
            this.birthDate = request.birthDate
            this.phoneNumber = phoneNumber.value
            this.address = address.value
            this.activeCollegiate = activeCollegiate.value
            this.identificationDocumentNumber = identificationDocumentNumber.value
            this.gender = genderEntity
            this.identificationDocumentType = documentTypeEntity
            this.specialty = specialtyEntity
        }

        doctorRepository.save(entity)

        val generatedId = LongValueObject.valueOf(entity.id)
        generatedId.validate()

        return generatedId.value
    }
}
