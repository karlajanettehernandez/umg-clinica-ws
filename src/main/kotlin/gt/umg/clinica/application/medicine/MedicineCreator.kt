package gt.umg.clinica.application.medicine

import gt.umg.clinica.domain.entities.Medicine
import gt.umg.clinica.domain.exceptions.ValidationException
import gt.umg.clinica.domain.repository.MedicineRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class MedicineCreator @Autowired constructor(private val medicineRepository: MedicineRepository) {

    fun getOrCreate(id: Long, name: String): Medicine {
        return if (id == 0L && name.isNotEmpty()) {
            create(name)
        } else {
            medicineRepository
                .findById(id)
                .orElseThrow { ValidationException("La entidad Medicine no existe") }
        }
    }

    private fun create(name: String): Medicine {
        val existingEntity = medicineRepository.findByName(name)

        if (existingEntity != null) return existingEntity

        val newEntity = Medicine.build {
            this.name = name
        }

        medicineRepository.save(newEntity)

        return newEntity
    }

}
