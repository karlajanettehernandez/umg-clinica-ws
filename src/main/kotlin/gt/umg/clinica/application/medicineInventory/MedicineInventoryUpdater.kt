package gt.umg.clinica.application.medicineInventory

import gt.umg.clinica.domain.exceptions.ValidationException
import gt.umg.clinica.domain.repository.MedicineInventoryRepository
import gt.umg.clinica.domain.repository.MedicineRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class MedicineInventoryUpdater @Autowired constructor(
    private val medicineInventoryRepository: MedicineInventoryRepository,
    private val medicineRepository: MedicineRepository
) {

    fun update(request: MedicineInventoryRequest) {
        val medicineId = request.medicineId ?: throw ValidationException("Medicine ID is required")
        val medicineEntity = medicineRepository
            .findById(medicineId)
            .orElseThrow { ValidationException("Medicine not found") }

        val entity = medicineInventoryRepository
            .findById(request.id ?: 0L)
            .orElseThrow { ValidationException("Entidad ${request.id} no existe") }

        entity.apply {
            this.purchasePrice = request.purchasePrice
            this.salePrice = request.salePrice
            this.quantity = request.quantity
            this.stock = (request.quantity ?: 0) - (entity.quantitySale ?: 0)
            this.batchNumber = request.batchNumber
            this.expirationDate = request.expirationDate
            this.active = request.active ?: false
            this.medicine = medicineEntity
        }

        medicineInventoryRepository.save(entity)
    }

}
