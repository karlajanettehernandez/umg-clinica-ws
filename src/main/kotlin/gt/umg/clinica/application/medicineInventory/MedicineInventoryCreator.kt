package gt.umg.clinica.application.medicineInventory

import gt.umg.clinica.application.medicine.MedicineCreator
import gt.umg.clinica.domain.entities.MedicineInventory
import gt.umg.clinica.domain.repository.MedicineInventoryRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class MedicineInventoryCreator @Autowired constructor(
    private val medicineInventoryRepository: MedicineInventoryRepository,
    private val medicineCreator: MedicineCreator
) {

    fun create(request: MedicineInventoryRequest): Long {
        val medicineId = request.medicineId ?: 0L
        val medicineName = request.medicineName ?: ""
        val medicineEntity = medicineCreator.getOrCreate(medicineId, medicineName)

        val entity = MedicineInventory.build {
            this.purchasePrice = request.purchasePrice
            this.salePrice = request.salePrice
            this.quantity = request.quantity
            this.quantitySale = 0
            this.stock = request.quantity
            this.batchNumber = request.batchNumber
            this.expirationDate = request.expirationDate
            this.medicine = medicineEntity
        }

        medicineInventoryRepository.save(entity)

        return entity.id ?: 0L
    }

}
