package gt.umg.clinica.application.medicineInventory

import java.math.BigDecimal
import java.time.LocalDate

class MedicineInventoryRequest {
    var id: Long? = null
    var purchasePrice: BigDecimal? = null
    var salePrice: BigDecimal? = null
    var quantity: Int? = null
    var batchNumber: String? = null
    var expirationDate: LocalDate? = null
    var medicineId: Long? = null
    var medicineName: String? = null
    var active: Boolean? = null

    companion object {
        fun build(init: MedicineInventoryRequest.() -> Unit) = MedicineInventoryRequest().apply(init)
    }
}
