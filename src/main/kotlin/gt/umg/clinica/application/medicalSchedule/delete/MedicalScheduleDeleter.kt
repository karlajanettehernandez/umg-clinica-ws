package gt.umg.clinica.application.medicalSchedule.delete

import gt.umg.clinica.domain.exceptions.ValidationException
import gt.umg.clinica.domain.validators.LongValueObject
import gt.umg.clinica.domain.repository.MedicalScheduleRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class MedicalScheduleDeleter @Autowired constructor(
    private val medicalScheduleRepository: MedicalScheduleRepository
) {
    fun delete(id: Long) {
        val entityId = LongValueObject.valueOf(id)
        entityId.validate()

        val entity = medicalScheduleRepository
            .findById(entityId.value)
            .orElseThrow { ValidationException("La entidad $id no existe") }

        entity.active = false

        medicalScheduleRepository.save(entity)
    }
}
