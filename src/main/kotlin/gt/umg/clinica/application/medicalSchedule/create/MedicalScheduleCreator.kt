package gt.umg.clinica.application.medicalSchedule.create

import gt.umg.clinica.application.medicalSchedule.MedicalScheduleRequest
import gt.umg.clinica.domain.exceptions.ValidationException
import gt.umg.clinica.domain.shared.DomainConstants
import gt.umg.clinica.domain.validators.LongValueObject
import gt.umg.clinica.domain.validators.StringValueObject
import gt.umg.clinica.domain.entities.MedicalSchedule
import gt.umg.clinica.domain.repository.DoctorRepository
import gt.umg.clinica.domain.repository.MedicalScheduleRepository
import gt.umg.clinica.domain.repository.MedicalScheduleStatusRepository
import gt.umg.clinica.domain.repository.PatientRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class MedicalScheduleCreator @Autowired constructor(
    private val medicalScheduleRepository: MedicalScheduleRepository,
    private val medicalScheduleStatusRepository: MedicalScheduleStatusRepository,
    private val doctorRepository: DoctorRepository,
    private val patientRepository: PatientRepository
) {
    fun create(request: MedicalScheduleRequest): Long {
        val doctorId = LongValueObject.valueOf(request.doctorId)
        val patientId = LongValueObject.valueOf(request.patientId)
        val patientName = StringValueObject.valueOf(request.patientName)
        val note = StringValueObject.valueOf(request.note)
        val startAt = request.startAt
        val endAt = request.endAt

        doctorId.validate()

        // 1. Validamos que tenga no tenga otra cita agendada en ese rango de horas
//        val exists = medicalScheduleRepository.existsByDoctorIdAndStartAtBetween(doctorId, startAt, endAt)
//
//        if (exists) {
//            throw ValidationException("Ya tiene citas agendadas en este horario, por favor seleccione otro.")
//        }

        val statusEntity = medicalScheduleStatusRepository
            .findByCode(DomainConstants.AGENDADO_MEDICAL_SCHEDULE_STATUS)
            ?: throw ValidationException("El estado agendado no existe")

        val doctorEntity = doctorRepository
            .findById(doctorId.value)
            .orElseThrow { ValidationException("Medico no existe") }

        val patientEntity = if (patientId.value > 0) {
            patientRepository
                .findById(patientId.value)
                .orElseThrow { ValidationException("Paciente no existe") }
        } else null

        val entity = MedicalSchedule.build {
            this.note = note.value
            this.startAt = startAt
            this.endAt = endAt
            this.patientName = patientName.value
            this.medicalScheduleStatus = statusEntity
            this.doctor = doctorEntity
            this.patient = patientEntity
        }

        medicalScheduleRepository.save(entity)

        val id = LongValueObject.valueOf(entity.id)
        id.validate()

        return id.value
    }
}
