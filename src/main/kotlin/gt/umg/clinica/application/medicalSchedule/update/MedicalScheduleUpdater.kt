package gt.umg.clinica.application.medicalSchedule.update

import gt.umg.clinica.application.medicalSchedule.MedicalScheduleRequest
import gt.umg.clinica.domain.exceptions.ValidationException
import gt.umg.clinica.domain.validators.LongValueObject
import gt.umg.clinica.domain.validators.StringValueObject
import gt.umg.clinica.domain.repository.DoctorRepository
import gt.umg.clinica.domain.repository.MedicalScheduleRepository
import gt.umg.clinica.domain.repository.PatientRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class MedicalScheduleUpdater @Autowired constructor(
    private val medicalScheduleRepository: MedicalScheduleRepository,
    private val doctorRepository: DoctorRepository,
    private val patientRepository: PatientRepository
) {
    fun update(request: MedicalScheduleRequest) {
        val id = LongValueObject.valueOf(request.id)
        val doctorId = LongValueObject.valueOf(request.doctorId)
        val patientId = LongValueObject.valueOf(request.patientId)
        val patientName = StringValueObject.valueOf(request.patientName)
        val note = StringValueObject.valueOf(request.note)
        val startAt = request.startAt
        val endAt = request.endAt

        val entityToUpdate = medicalScheduleRepository
            .findById(id.value)
            .orElseThrow { ValidationException("La entidad $id no existe") }

        val doctorEntity = doctorRepository
            .findById(doctorId.value)
            .orElseThrow { ValidationException("Medico no existe") }

        val patientEntity = if (patientId.value > 0) {
            patientRepository
                .findById(patientId.value)
                .orElseThrow { ValidationException("Paciente no existe") }
        } else null

        entityToUpdate.apply {
            this.note = note.value
            this.startAt = startAt
            this.endAt = endAt
            this.patientName = patientName.value
            this.doctor = doctorEntity
            this.patient = patientEntity
        }

        medicalScheduleRepository.save(entityToUpdate)
    }
}
