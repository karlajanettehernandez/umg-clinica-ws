package gt.umg.clinica.application.medicalSchedule

import java.time.LocalDateTime

class MedicalScheduleRequest {
    var id: Long = 0L
    var note: String = ""
    var startAt: LocalDateTime = LocalDateTime.now()
    var endAt: LocalDateTime = LocalDateTime.now()
    var medicalScheduleStatusId: Long = 0L
    var doctorId: Long = 0L
    var patientId: Long = 0L
    var patientName: String = ""

    companion object {
        fun build(init: MedicalScheduleRequest.() -> Unit) = MedicalScheduleRequest().apply(init)
    }
}
