package gt.umg.clinica.domain.shared

object DomainConstants {
    const val EMAIL_REGEX = "^\\S+@\\S+\\.\\S+\$"
    const val AGENDADO_MEDICAL_SCHEDULE_STATUS = "01"
}
