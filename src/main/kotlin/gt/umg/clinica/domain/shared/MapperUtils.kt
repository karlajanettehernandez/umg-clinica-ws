package gt.umg.clinica.domain.shared

import gt.umg.clinica.domain.entities.Doctor
import gt.umg.clinica.domain.entities.Patient
import java.time.LocalDate
import java.time.Period

object MapperUtils {
    fun getDoctorName(doctor: Doctor?): String {
        val doctorName = doctor?.name ?: ""
        val doctorLastName = doctor?.lastName ?: ""
        val gender = doctor?.gender?.name ?: ""

        val name = "$doctorName $doctorLastName"

        return if (gender == "Masculino") "Dr. $name" else "Dra. $name"
    }

    fun getPatientName(patient: Patient?): String {
        val name = patient?.name ?: ""
        val lastName = patient?.lastName ?: ""

        return "$name $lastName"
    }

    fun getAge(birthDate: LocalDate?): String {
        if (birthDate == null) return ""

        val currentDate = LocalDate.now()
        val currentYear = currentDate.year
        val yearOfBirth = birthDate.year

        return when (val age = currentYear - yearOfBirth) {
            0 -> getAgeInMonths(birthDate)
            1 -> "$age año"
            else -> "$age años"
        }
    }

    private fun getAgeInMonths(birthDate: LocalDate): String {
        val currentDate = LocalDate.now()
        val period = Period.between(birthDate, currentDate)

        return when (val months = period.months) {
            1 -> "$months mes"
            else -> "$months meses"
        }
    }
}
