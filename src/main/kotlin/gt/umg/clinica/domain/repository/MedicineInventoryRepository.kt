package gt.umg.clinica.domain.repository

import gt.umg.clinica.domain.entities.MedicineInventory
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface MedicineInventoryRepository : JpaRepository<MedicineInventory, Long> {

    @Query(
        value = "from MedicineInventory as t0 " +
                " where t0.active = true " +
                " and t0.stock > 0 " +
                " order by t0.createdAt desc "
    )
    fun findAllActiveTrueAndStockGreaterThanZero(): List<MedicineInventory>

    @Query(
        value = "from MedicineInventory as t0 " +
                " order by t0.createdAt desc "
    )
    fun findallOrderByCreatedAtDesc(): List<MedicineInventory>

}
