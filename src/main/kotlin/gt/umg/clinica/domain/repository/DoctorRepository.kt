package gt.umg.clinica.domain.repository

import gt.umg.clinica.domain.entities.Doctor
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
interface DoctorRepository : JpaRepository<Doctor, Long> {

    @Query(
        value = " from Doctor as t0 " +
                " where t0.active = true " +
                " order by t0.lastName asc "
    )
    fun findAllActive(): List<Doctor>

    @Query(
        value = " from Doctor as t0 " +
                " order by t0.createdAt asc "
    )
    fun findAllOrderByCreatedAtDesc(): List<Doctor>

    @Query(
        value = " select count(t0) > 0 " +
                " from Doctor as t0 " +
                " where t0.identificationDocumentNumber = :identificationDocumentNumber " +
                " and t0.active = true "
    )
    fun existsByIdentificationDocumentNumber(
        @Param(value = "identificationDocumentNumber") identificationDocumentNumber: String
    ): Boolean


    @Query(
        value = " select count(t0) > 0 " +
                " from Doctor as t0 " +
                " where t0.identificationDocumentNumber = :identificationDocumentNumber " +
                " and t0.id != :id " +
                " and t0.active = true "
    )
    fun existsByIdentificationDocumentNumberAndIdNotEquals(
        @Param(value = "identificationDocumentNumber") identificationDocumentNumber: String,
        @Param(value = "id") id: Long
    ): Boolean

    @Query(
        value = " select count(t0) > 0 " +
                " from Doctor as t0 " +
                " where t0.activeCollegiate = :activeCollegiate " +
                " and t0.active = true "
    )
    fun existsByActiveCollegiate(
        @Param(value = "activeCollegiate") activeCollegiate: String
    ): Boolean


    @Query(
        value = " select count(t0) > 0 " +
                " from Doctor as t0 " +
                " where t0.activeCollegiate = :activeCollegiate " +
                " and t0.id != :id " +
                " and t0.active = true "
    )
    fun existsByActiveCollegiateAndIdNotEquals(
        @Param(value = "activeCollegiate") activeCollegiate: String,
        @Param(value = "id") id: Long
    ): Boolean

}
