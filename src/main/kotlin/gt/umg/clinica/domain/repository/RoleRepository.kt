package gt.umg.clinica.domain.repository

import gt.umg.clinica.domain.entities.Role
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface RoleRepository: JpaRepository<Role, Long> {

    fun findByCode(code: String): Role?

}
