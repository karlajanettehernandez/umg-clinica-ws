package gt.umg.clinica.domain.repository

import gt.umg.clinica.domain.entities.MedicalHistoryPrescription
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
interface MedicalHistoryPrescriptionRepository : JpaRepository<MedicalHistoryPrescription, Long> {

    @Query(
        value = "from MedicalHistoryPrescription as t0 " +
                " where t0.medicalHistory.id = :medicalHistoryId " +
                " and t0.active = true " +
                " order by t0.id "
    )
    fun findByMedicalHistoryIdAndActiveTrue(@Param(value = "medicalHistoryId") medicalHistoryId: Long): List<MedicalHistoryPrescription>

    @Query(
        value = "from MedicalHistoryPrescription as t0 " +
                " where t0.medicalHistory.id = :medicalHistoryId "
    )
    fun findByMedicalHistoryId(@Param(value = "medicalHistoryId") medicalHistoryId: Long): List<MedicalHistoryPrescription>

}
