package gt.umg.clinica.domain.repository

import gt.umg.clinica.domain.entities.Specialty
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface SpecialtyRepository : JpaRepository<Specialty, Long> {
}
