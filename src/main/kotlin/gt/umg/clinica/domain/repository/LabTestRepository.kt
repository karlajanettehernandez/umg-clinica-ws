package gt.umg.clinica.domain.repository

import gt.umg.clinica.domain.entities.LabTest
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface LabTestRepository : JpaRepository<LabTest, Long> {

    @Query(
        value = "from LabTest  as t0 " +
                " where t0.active = true " +
                " order by t0.name"
    )
    fun findAllActiveTrue(): List<LabTest>

}