package gt.umg.clinica.domain.repository

import gt.umg.clinica.domain.entities.DocumentType
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
interface DocumentTypeRepository : JpaRepository<DocumentType, Long> {

    @Query(value = "from DocumentType where code = :code")
    fun findByCode(@Param(value = "code") code: String): DocumentType?

}
