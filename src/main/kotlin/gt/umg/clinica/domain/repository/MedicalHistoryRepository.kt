package gt.umg.clinica.domain.repository

import gt.umg.clinica.domain.entities.MedicalHistory
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import java.time.LocalDateTime

@Repository
interface MedicalHistoryRepository : JpaRepository<MedicalHistory, Long> {

    @Query(
        value = "from MedicalHistory as t0 " +
                " where t0.patient.id = :patientId " +
                " and t0.active = true " +
                " order by t0.createdAt desc "
    )
    fun findByPatientIdAndActiveTrue(@Param(value = "patientId") patientId: Long): List<MedicalHistory>

    @Query(
        value = "select count(distinct t0.patient.id)" +
                " from MedicalHistory as t0 " +
                " where t0.createdAt between :startDate and :endDate"
    )
    fun countDistinctByCreatedAtBetween(
        @Param(value = "startDate") startDate: LocalDateTime,
        @Param(value = "endDate") endDate: LocalDateTime
    ): Long

}
