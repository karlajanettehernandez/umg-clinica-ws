package gt.umg.clinica.domain.repository

import gt.umg.clinica.domain.entities.Medicine
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
interface MedicineRepository : JpaRepository<Medicine, Long> {

    @Query(
        value = "from Medicine as t0 " +
                " where t0.active = true " +
                " order by t0.name"
    )
    fun findAllActiveTrue(): List<Medicine>

    @Query(value = "from Medicine as t0 where lower(t0.name) = lower(:name) ")
    fun findByName(@Param(value = "name") name: String): Medicine?

}
