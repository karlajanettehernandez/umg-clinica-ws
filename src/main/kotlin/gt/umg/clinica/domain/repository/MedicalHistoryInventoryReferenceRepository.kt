package gt.umg.clinica.domain.repository

import gt.umg.clinica.domain.entities.MedicalHistoryInventoryReference
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
interface MedicalHistoryInventoryReferenceRepository : JpaRepository<MedicalHistoryInventoryReference, Long> {

    @Query(
        value = "from MedicalHistoryInventoryReference as t0 " +
                " where t0.active = true " +
                " and t0.medicalHistory.id = :medicalHistoryId "
    )
    fun findByMedicalHistoryIdAndActiveTrue(@Param(value = "medicalHistoryId") medicalHistoryId: Long): List<MedicalHistoryInventoryReference>

    @Query(
        value = "from MedicalHistoryInventoryReference as t0 " +
                " where t0.active = true "
    )
    fun findByMedicalHistoryId(@Param(value = "medicalHistoryId") medicalHistoryId: Long): List<MedicalHistoryInventoryReference>

}
