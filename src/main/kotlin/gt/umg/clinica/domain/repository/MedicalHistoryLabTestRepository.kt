package gt.umg.clinica.domain.repository

import gt.umg.clinica.domain.entities.MedicalHistoryLabTest
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
interface MedicalHistoryLabTestRepository : JpaRepository<MedicalHistoryLabTest, Long> {

    @Query(
        value = "from MedicalHistoryLabTest as t0 " +
                " where t0.medicalHistory.id = :medicalHistoryId " +
                " and t0.active = true " +
                " order by t0.id "
    )
    fun findByMedicalHistoryIdAndActiveTrue(@Param(value = "medicalHistoryId") medicalHistoryId: Long): List<MedicalHistoryLabTest>

    @Query(
        value = "from MedicalHistoryLabTest as t0 " +
                " where t0.medicalHistory.id = :medicalHistoryId "
    )
    fun findByMedicalHistoryId(@Param(value = "medicalHistoryId") medicalHistoryId: Long): List<MedicalHistoryLabTest>

}
