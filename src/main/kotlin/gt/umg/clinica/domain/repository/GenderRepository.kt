package gt.umg.clinica.domain.repository

import gt.umg.clinica.domain.entities.Gender
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface GenderRepository : JpaRepository<Gender, Long> {
}
