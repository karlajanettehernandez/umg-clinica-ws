package gt.umg.clinica.domain.repository

import gt.umg.clinica.domain.entities.Patient
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
interface PatientRepository : JpaRepository<Patient, Long> {

    @Query(
        value = " from Patient as t0 " +
                " where t0.active = true " +
                " order by t0.lastName asc "
    )
    fun findAllActive(): List<Patient>

    @Query(
        value = " from Patient as t0 " +
                " order by t0.createdAt asc "
    )
    fun findAllOrderByCreatedAtDesc(): List<Patient>

    @Query(
        value = " select count(t0) > 0 " +
                " from Patient as t0 " +
                " where t0.identificationDocumentNumber = :identificationDocumentNumber " +
                " and t0.active = true "
    )
    fun existsByIdentificationDocumentNumber(
        @Param(value = "identificationDocumentNumber") identificationDocumentNumber: String
    ): Boolean


    @Query(
        value = " select count(t0) > 0 " +
                " from Patient as t0 " +
                " where t0.identificationDocumentNumber = :identificationDocumentNumber " +
                " and t0.id != :id " +
                " and t0.active = true "
    )
    fun existsByIdentificationDocumentNumberAndIdNotEquals(
        @Param(value = "identificationDocumentNumber") identificationDocumentNumber: String,
        @Param(value = "id") id: Long
    ): Boolean

}
