package gt.umg.clinica.domain.repository

import gt.umg.clinica.domain.entities.MedicalScheduleStatus
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface MedicalScheduleStatusRepository : JpaRepository<MedicalScheduleStatus, Long> {

    @Query(value = "from MedicalScheduleStatus as t0" +
            " where t0.code = :code " +
            " and t0.active = true ")
    fun findByCode(code: String): MedicalScheduleStatus?

}
