package gt.umg.clinica.domain.repository

import gt.umg.clinica.domain.entities.MedicalSchedule
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import java.time.LocalDateTime

@Repository
interface MedicalScheduleRepository : JpaRepository<MedicalSchedule, Long> {

    @Query(
        value = " select count(t0) > 0 " +
                " from MedicalSchedule as t0 " +
                " where t0.doctor.id = :doctorId " +
                " and t0.startAt between :startDate and :endDate "
    )
    fun existsByDoctorIdAndStartAtBetween(
        @Param(value = "doctorId") doctorId: Long,
        @Param(value = "startDate") startDate: LocalDateTime,
        @Param(value = "endDate") endDate: LocalDateTime
    ): Boolean

    @Query(value = "from MedicalSchedule as t0 " +
            " where t0.active = true " +
            " order by t0.createdAt desc ")
    fun findAllActiveTrue(): List<MedicalSchedule>

}
