package gt.umg.clinica.domain.entities

import jakarta.persistence.Entity
import jakarta.persistence.Table

@Entity
@Table(name = "document_type", schema = "person")
class DocumentType : HibernateBaseEntity() {
    var code: String? = null
    var name: String? = null
}
