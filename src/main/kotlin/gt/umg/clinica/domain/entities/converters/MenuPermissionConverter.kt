package gt.umg.clinica.domain.entities.converters

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.type.TypeFactory
import gt.umg.clinica.domain.entities.MenuPermission
import jakarta.persistence.AttributeConverter


class MenuPermissionConverter : AttributeConverter<List<MenuPermission>, String> {

    private val objectMapper = ObjectMapper()

    override fun convertToDatabaseColumn(dataList: List<MenuPermission>?): String {
        if (dataList == null) return ""
        return objectMapper.writeValueAsString(dataList)
    }

    override fun convertToEntityAttribute(jsonList: String?): List<MenuPermission> {
        if (jsonList == null) return listOf()

        val type = TypeFactory
            .defaultInstance()
            .constructCollectionType(List::class.java, MenuPermission::class.java)

        return objectMapper.readValue(jsonList, type)
    }
}
