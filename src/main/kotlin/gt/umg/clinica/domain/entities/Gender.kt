package gt.umg.clinica.domain.entities

import jakarta.persistence.Entity
import jakarta.persistence.Table

@Entity
@Table(name = "gender", schema = "person")
class Gender : HibernateBaseEntity() {
    var name: String? = null
}
