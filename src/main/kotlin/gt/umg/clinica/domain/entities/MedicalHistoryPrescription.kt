package gt.umg.clinica.domain.entities

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.FetchType
import jakarta.persistence.JoinColumn
import jakarta.persistence.ManyToOne
import jakarta.persistence.Table

@Entity
@Table(name = "medical_history_prescription", schema = "clinic")
class MedicalHistoryPrescription : HibernateBaseEntity() {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "medical_history_id")
    var medicalHistory: MedicalHistory? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "medicine_id")
    var medicine: Medicine? = null

    @Column(columnDefinition = "text")
    var indications: String? = null

    companion object {
        fun build(init: MedicalHistoryPrescription.() -> Unit) =
            MedicalHistoryPrescription().apply(init)
    }

}
