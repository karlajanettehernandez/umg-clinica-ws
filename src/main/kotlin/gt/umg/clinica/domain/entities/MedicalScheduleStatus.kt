package gt.umg.clinica.domain.entities

import jakarta.persistence.Entity
import jakarta.persistence.Table

@Entity
@Table(name = "medical_schedule_status", schema = "clinic")
class MedicalScheduleStatus : HibernateBaseEntity() {
    var code: String? = null
    var name: String? = null
}
