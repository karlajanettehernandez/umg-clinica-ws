package gt.umg.clinica.domain.entities

import jakarta.persistence.Entity
import jakarta.persistence.ManyToOne
import jakarta.persistence.Table

@Entity
@Table(name = "user_role", schema = "usr")
class UserRole : HibernateBaseEntity() {

    @ManyToOne
    var user: User? = null

    @ManyToOne
    var role: Role? = null

    companion object {
        fun build(init: UserRole.() -> Unit) = UserRole().apply(init)
    }

}
