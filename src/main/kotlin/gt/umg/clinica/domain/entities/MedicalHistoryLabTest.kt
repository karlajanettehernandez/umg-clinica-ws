package gt.umg.clinica.domain.entities

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.FetchType
import jakarta.persistence.JoinColumn
import jakarta.persistence.ManyToOne
import jakarta.persistence.Table

@Entity
@Table(name = "medical_history_lab_test", schema = "clinic")
class MedicalHistoryLabTest : HibernateBaseEntity() {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "medical_history_id")
    var medicalHistory: MedicalHistory? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "lab_test_id")
    var labTest: LabTest? = null

    @Column(columnDefinition = "text")
    var description: String? = null

    companion object {
        fun build(init: MedicalHistoryLabTest.() -> Unit) = MedicalHistoryLabTest().apply(init)
    }

}
