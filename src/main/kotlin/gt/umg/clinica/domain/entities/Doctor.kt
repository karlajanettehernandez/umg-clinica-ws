package gt.umg.clinica.domain.entities

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.FetchType
import jakarta.persistence.JoinColumn
import jakarta.persistence.ManyToOne
import jakarta.persistence.Table
import java.time.LocalDate

@Entity
@Table(name = "doctor", schema = "clinic")
class Doctor : HibernateBaseEntity() {
    var name: String? = null

    @Column(name = "last_name")
    var lastName: String? = null

    var email: String? = null

    @Column(name = "birth_date")
    var birthDate: LocalDate? = null

    @ManyToOne(fetch = FetchType.LAZY)
    var gender: Gender? = null

    @Column(name = "phone_number")
    var phoneNumber: String? = null

    var address: String? = null

    @Column(name = "active_collegiate")
    var activeCollegiate: String? = null

    var identificationDocumentNumber: String? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "identification_document_type_id")
    var identificationDocumentType: DocumentType? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "specialty_id")
    var specialty: Specialty? = null

    companion object {
        fun build(init: Doctor.() -> Unit) = Doctor().apply(init)
    }
}
