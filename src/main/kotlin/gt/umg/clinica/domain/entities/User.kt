package gt.umg.clinica.domain.entities

import jakarta.persistence.Entity
import jakarta.persistence.ManyToOne
import jakarta.persistence.Table

@Entity
@Table(name = "user", schema = "usr")
class User : HibernateBaseEntity() {
    var name: String? = ""
    var email: String? = ""
    var password: String? = ""

    @ManyToOne
    var doctor: Doctor? = null

    companion object {
        fun build(init: User.() -> Unit) = User().apply(init)
    }
}
