package gt.umg.clinica.domain.entities

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.ManyToOne
import jakarta.persistence.Table

@Entity
@Table(name = "medical_history", schema = "clinic")
class MedicalHistory : HibernateBaseEntity() {

    @ManyToOne
    var doctor: Doctor? = null

    @ManyToOne
    var patient: Patient? = null

    @Column(columnDefinition = "text")
    var description: String? = null

    @Column(columnDefinition = "text")
    var diagnostic: String? = null

    companion object {
        fun build(init: MedicalHistory.() -> Unit) = MedicalHistory().apply(init)
    }

}
