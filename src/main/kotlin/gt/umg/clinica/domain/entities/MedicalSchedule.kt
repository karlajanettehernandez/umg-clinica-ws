package gt.umg.clinica.domain.entities

import java.time.LocalDateTime
import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.FetchType
import jakarta.persistence.JoinColumn
import jakarta.persistence.ManyToOne
import jakarta.persistence.Table

@Entity
@Table(name = "medical_schedule", schema = "clinic")
class MedicalSchedule : HibernateBaseEntity() {

    @Column(name = "patient_name", length = 100)
    var patientName: String? = null

    @Column(name = "note", length = 500)
    var note: String? = null

    @Column(name = "start_at")
    var startAt: LocalDateTime? = LocalDateTime.now()

    @Column(name = "end_at")
    var endAt: LocalDateTime? = LocalDateTime.now()

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "medical_schedule_status_id")
    var medicalScheduleStatus: MedicalScheduleStatus? = null

    @ManyToOne(fetch = FetchType.LAZY)
    var doctor: Doctor? = null

    @ManyToOne(fetch = FetchType.LAZY)
    var patient: Patient? = null

    companion object {
        fun build(init: MedicalSchedule.() -> Unit) = MedicalSchedule().apply(init)
    }

}
