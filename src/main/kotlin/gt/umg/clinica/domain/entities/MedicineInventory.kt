package gt.umg.clinica.domain.entities

import java.math.BigDecimal
import java.time.LocalDate
import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.FetchType
import jakarta.persistence.JoinColumn
import jakarta.persistence.ManyToOne
import jakarta.persistence.Table

@Entity
@Table(name = "medicine_inventory", schema = "clinic")
class MedicineInventory : HibernateBaseEntity() {

    @Column(name = "purchase_price")
    var purchasePrice: BigDecimal? = null

    @Column(name = "sale_price")
    var salePrice: BigDecimal? = null

    var quantity: Int? = 0

    @Column(name = "quantity_sale")
    var quantitySale: Int? = 0

    @Column(name = "batch_number")
    var batchNumber: String? = null

    @Column(name = "expiration_date")
    var expirationDate: LocalDate? = null

    var stock: Int? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "medicine_id")
    var medicine: Medicine? = null

    @Column(columnDefinition = "text")
    var comments: String? = null

    companion object {
        fun build(init: MedicineInventory.() -> Unit) = MedicineInventory().apply(init)
    }

}
