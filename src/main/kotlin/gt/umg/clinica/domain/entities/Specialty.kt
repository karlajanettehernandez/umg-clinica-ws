package gt.umg.clinica.domain.entities

import jakarta.persistence.Entity
import jakarta.persistence.Table

@Entity
@Table(name = "specialty", schema = "clinic")
class Specialty : HibernateBaseEntity() {
    var name: String? = null
}
