package gt.umg.clinica.domain.entities

import jakarta.persistence.Entity
import jakarta.persistence.Table

@Entity
@Table(name = "lab_test", schema = "clinic")
class LabTest : HibernateBaseEntity() {
    var name: String? = null
}
