package gt.umg.clinica.domain.entities

import jakarta.persistence.Entity
import jakarta.persistence.FetchType
import jakarta.persistence.JoinColumn
import jakarta.persistence.ManyToOne
import jakarta.persistence.Table

@Entity
@Table(name = "medical_history_inventory_reference", schema = "clinic")
class MedicalHistoryInventoryReference : HibernateBaseEntity() {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "medical_history_id")
    var medicalHistory: MedicalHistory? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "medicine_inventory_id")
    var medicineInventory: MedicineInventory? = null

    var quantity: Int? = null

    companion object {
        fun build(init: MedicalHistoryInventoryReference.() -> Unit) = MedicalHistoryInventoryReference().apply(init)
    }

}
