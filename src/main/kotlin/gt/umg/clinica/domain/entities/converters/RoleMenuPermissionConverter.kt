package gt.umg.clinica.domain.entities.converters

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.type.TypeFactory
import jakarta.persistence.AttributeConverter

class RoleMenuPermissionConverter : AttributeConverter<List<String>, String> {

    private val objectMapper = ObjectMapper()

    override fun convertToDatabaseColumn(dataList: List<String>?): String {
        if (dataList == null) return "[]"

        return objectMapper.writeValueAsString(dataList)
    }

    override fun convertToEntityAttribute(jsonList: String?): List<String> {
        if (jsonList == null) return listOf()

        val type = TypeFactory
            .defaultInstance()
            .constructCollectionType(List::class.java, String::class.java)

        return objectMapper.readValue(jsonList, type)
    }
}
