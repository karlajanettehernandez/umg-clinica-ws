package gt.umg.clinica.domain.entities

class MenuPermission {
    var description: String? = ""
    var code: String? = ""
}
