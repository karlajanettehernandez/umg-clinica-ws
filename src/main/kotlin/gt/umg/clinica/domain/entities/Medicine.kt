package gt.umg.clinica.domain.entities

import jakarta.persistence.Entity
import jakarta.persistence.Table

@Entity
@Table(name = "medicine", schema = "clinic")
class Medicine : HibernateBaseEntity() {
    var name: String? = null

    companion object {
        fun build(init: Medicine.() -> Unit) = Medicine().apply(init)
    }
}
