package gt.umg.clinica.domain.entities

import java.time.LocalDate
import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.FetchType
import jakarta.persistence.JoinColumn
import jakarta.persistence.ManyToOne
import jakarta.persistence.Table

@Entity
@Table(name = "patient", schema = "clinic")
class Patient : HibernateBaseEntity() {
    var name: String? = null

    @Column(name = "last_name")
    var lastName: String? = null

    var email: String? = null

    @Column(name = "birth_date")
    var birthDate: LocalDate? = LocalDate.now()

    @ManyToOne(fetch = FetchType.LAZY)
    var gender: Gender? = null

    @Column(name = "phone_number")
    var phoneNumber: String? = null

    var address: String? = null

    @Column(name = "identification_document_number")
    var identificationDocumentNumber: String? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "identification_document_type_id")
    var identificationDocumentType: DocumentType? = null

    companion object {
        fun build(init: Patient.() -> Unit) = Patient().apply(init)
    }

}
