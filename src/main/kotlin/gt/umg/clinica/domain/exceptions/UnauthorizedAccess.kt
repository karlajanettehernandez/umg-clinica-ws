package gt.umg.clinica.domain.exceptions

class UnauthorizedAccess(message: String = "No tiene acceso a este recurso") : Exception(message)
