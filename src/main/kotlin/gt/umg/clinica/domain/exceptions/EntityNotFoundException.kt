package gt.umg.clinica.domain.exceptions

class EntityNotFoundException(message: String): Exception(message)
