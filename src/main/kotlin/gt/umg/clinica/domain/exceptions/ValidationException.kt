package gt.umg.clinica.domain.exceptions

class ValidationException(message: String): Exception(message)
