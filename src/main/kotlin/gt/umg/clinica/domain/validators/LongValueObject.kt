package gt.umg.clinica.domain.validators

import gt.umg.clinica.domain.exceptions.ValidationException

class LongValueObject(val value: Long, private val errorMessage: String) {

    fun validate() {
        if (value <= 0L) {
            throw ValidationException(errorMessage)
        }
    }

    companion object {
        fun valueOf(value: Long?) = LongValueObject(value ?: 0L, "El campo id es requerido")
    }

}
