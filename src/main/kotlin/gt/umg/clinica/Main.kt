package gt.umg.clinica

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan
import org.springframework.data.jpa.repository.config.EnableJpaRepositories

@SpringBootApplication
@ComponentScan(
    value = [
        "gt.umg.clinica.api",
        "gt.umg.clinica.application",
        "gt.umg.clinica.domain"
    ]
)
@EnableJpaRepositories(basePackages = ["gt.umg.clinica.domain"])
class App

fun main(args: Array<String>) {
    runApplication<App>(*args)
}
